import { HttpModule } from '@angular/http';

import { Component, ViewChild, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonBaseService } from "../services/common.service";
import { DalService } from "../services/dal.service";
import { environment } from "../../environments/environment";
import { PopupComponent } from './../common/popup/popup.component';
import 'rxjs/add/operator/retry';


@Component({
  templateUrl: 'login.html',
})
export class LoginComponent {
	loginParams : any;

   //ViewChild for Fine Popup
    @ViewChild(PopupComponent)
     finePopup : PopupComponent;
    //ViewChild for Fine Popup --end --

	constructor(
        private commonserve:CommonBaseService,
        private modalService: NgbModal,
        private ref: ChangeDetectorRef,
        private router : Router,
        private dal : DalService

    ){
    if(this.commonserve.initReader){
        this.dal.initializeSdr10(environment.sdrInitializeVariable);
        this.dal.initializeBuaroh(environment.buarohinitializeVariable);

        this.commonserve.initReader = 0; 
    }
    this.loginParams = {
      "username" : "operator1r1",
      "password" : "12345678"
    }
	}
	login(){
   this.router.navigate(['/dashboard']);
	}
   updateStore (auth){
    let data = {
      "accesstoken" : auth.accessToken,
      "refreshtoken" : auth.refreshToken,
      "cartid" : ""
    };
    this.commonserve.setStoreData(data);
  }

}
