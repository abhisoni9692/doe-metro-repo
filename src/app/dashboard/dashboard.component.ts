import { Component, ViewChild, OnInit, ChangeDetectorRef, Input, ElementRef, ViewChildren, Output} from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { CommonBaseService } from "../services/common.service";
import { DalService } from "../services/dal.service";
import { environment } from "../../environments/environment";
import { Base64Service } from '../services/base64.service';
import { PopupComponent } from './../common/popup/popup.component';
import * as $ from 'jquery';
declare var $: any;
declare var require: any;

@Component({
    templateUrl: 'dashboard.html',
})
export class DashboardComponent {

    hide_First_Screen :boolean = false;
    hide_Second_Screen :boolean = true;
    vehicle_Cat_no : number = 1;
    totalScreen : number;
    closeResult: string;
    dal_path : any;
    cardInfo :any;
    holderInfo :any;
    tempCardInfo :any = {
        aadhaar: "",
        balance: "",
        cardNumber: "",
        name: "",
        mobile: "",
        vehicleNumber: "",
        vehicleType: "",
        passes : []
    };
    generic : any;
    ownername : string;
    lostCard_number : any;
    sdrInitializeVariable :any;
    buarohinitializeVariable : any;
    rechargeButton: any;
    activateButton: any;
    recharge_amount : any;
    backgroundBtn : any;
    digitBackgroundVar : any;
    payParams : any;
    rechargeParams : any;
    //modal popup variable
    tempAction : any;
    popup : any;
    //time vaiable
    currentDateTime : any;
    dataInEpoc: any;
    //aadhar blur
    editAadharFlag : any;
    //transaction var
        activityCodeApi : any; 
    userid : any;
    collectionVehicleTypeNumCode : any;
    collectionVehicleCatNumCode : any;
    vehicleTypeArray : any
    vehicleCatArray : any;
    selectVehicle : any;
    readerMode : any;
    validClick : any;
    //ViewChild for Fine Popup
    @ViewChild(PopupComponent)
     finePopup : PopupComponent;

    constructor(
    private commonserve:CommonBaseService,
    private ref: ChangeDetectorRef,
    private base64 : Base64Service,
    private router : Router,
    private dal : DalService,  
    ){   
        //buaroh vaiable
        this.payParams = {
            "serviceType":"TOLL",
            "action":"PAY",
            "posTerminalId":"123456789",
            "amount": 0,
            "sourceBranchId" : "123456789",
            "destinationBranchId" : "12",
            "timeInMilliSeconds": '1578632680878',
            "readerType": environment.buarohinitializeVariable.readerType,
            "readerIndex":0
        }  
        this.ownername = "";
        this.holderInfo = {
            "vehicleNo" : "",
            "cardNo" : "",
            "holderName" : "",
            "phoneNo" : "",
            "aadharNo" : "",
            "rfidNo" : "",
            "currentAmount" : "",
        };

        this.generic = {
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : true,
            "activateDisable" : true,
            "updateDisable" : true,
            "lostCardActive" : false,
            "clearallDisable" :true,
            "rechargeInitited" : false,
            "updateInitited" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "activeVehicleClass" : true,
          //  "nextVehicleName" : true
        };
        this.recharge_amount = "";
        this.lostCard_number = "";
        this.backgroundBtn = {
            "activatebackground" : false,
            "rechargebackground" : false,
            "updatebackground" :false,
        }
        //use pass variable

        //stop all readers and define defaults and then intialize all
        this.stopallReadersAndDefineDefaults();
    }

    stopallReadersAndDefineDefaults(){
        console.log("comes in stopallReadersAndDefineDefaults method")
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultsdr) => {
            console.log("sdr10 stopDetection in beginning in sdr10 stopallReadersAndDefineDefaults",resultsdr) 
            this.dal.stopDetection(environment.buarohinitializeVariable, (resultbuaroh) => {
                 console.log("sdr10 stopDetection in beginning in buaroh1 stopallReadersAndDefineDefaults",resultbuaroh) 
                this.definecardDefaults();
            });
        });
    }
    //restrict character or number function ends
    onlyNumberKey(event) {
        this.editAadharFlag = true;
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

    restrictNumeric(e) {
        return (e.charCode == 8 || e.charCode == 0 || e.charCode == 32) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90;
    }
    restrictSpecialcharachter(e) {
        return (e.charCode == 8 || e.charCode == 0) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90 || e.charCode >= 48 && e.charCode <= 57;
    }
    //restrict character or number function ends
    enterFromKeyboard(e, popup){
        if(e.charCode == 13)
        {
           this.doneButtonClick(popup);
           this.validClick = 1;
        }
    }
    numberButtonClick(rate, type) { 
        if(this.validClick !== 1){
            if (this.recharge_amount.toString().length < 6) {
                let temp = this.recharge_amount + rate;
                this.recharge_amount = parseInt(temp);
                this.digitBackgroundVar = true;
            }
        }
        this.validClick = 0;
    };
    rateButtonClick(rate, type) {
        this.recharge_amount = rate;
    };
    previousScreen(){
        this.hide_Second_Screen = true;
        this.hide_First_Screen = false;
        this.vehicle_Cat_no = 1;
        this.totalScreen = 1;
    }
    nextScreen(){
        this.hide_Second_Screen = false;
        this.hide_First_Screen = true;
        this.vehicle_Cat_no = 2;
        this.totalScreen = 2;
    }
    //changeaadhar mode
    changeAadharMode(flag) {
        this.editAadharFlag = flag;
        this.ref.detectChanges();
    }
    //clear all button action
    clearAll() {
        this.holderInfo = {
            "vehicleNo" : "",
            "cardNo" : "",
            "holderName" : "",
            "phoneNo" : "",
            "aadharNo" : "",
            "rfidNo" : "",
            "currentAmount" : "",
        };
        this.lostCard_number = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard(); 
    }
      //Successfully done popup
    donePopup(message){
        this.finePopup.showPopup(message);
        setTimeout(()=> {
            this.finePopup.closePopup();

        },1000);
    }
    //successfully done popup ends

    //card function starts to call defualt card display
    definecardDefaults() {
        console.log("setting all the date in ui")
        let currentUrl = this.router.url;
        if(currentUrl == '/dashboard'){
            this.payParams.amount = "";
            this.cardInfo = "";
            this.holderInfo = {
                "vehicleNo" : "",
                "cardNo" : "",
                "holderName" : "",
                "phoneNo" : "",
                "aadharNo" : "",
                "rfidNo" : "",
                "currentAmount" : "",
            };
            this.lostCard_number = "";
            this.generic = {
                "rechargeInitited" : false,
                "updateInitited" : false,
                "activateInitited" : false,
                "rechargeDisable" : true,
                "activateDisable" : true,
                "updateDisable" : true,
                "rechargeamountDisable" : true,
                "doneDisable" : true,
                "vehiclenoActive" : true,
                "nameActive" : true,
                "mobilenoActive" : true,
                "cardnoActive" : true,
                "aadharnoActive" : true,
                "rfidActive" : true,
                "lostCardActive" : false,
                "clearallDisable" :true,
            };
            this.recharge_amount = "";
            this.backgroundBtn = {
                "activatebackground" : false,
                "rechargebackground" : false,
                "updatebackground" :false,
            }
            this.ref.detectChanges();
            this.getGenericDataFromCard(); 
        }
    }
    //display generic data function
    genericAction(calback) {
        this.editAadharFlag = false;
        this.holderInfo = {
            "vehicleNo" : this.cardInfo.vehicleNumber,
            "cardNo" : this.cardInfo.cardNumber,
            "holderName" : this.cardInfo.name,
            "phoneNo" : this.cardInfo.mobile,
            "aadharNo" : this.cardInfo.aadhaar,
            "rfidNo" : "",
            "currentAmount" : this.cardInfo.balance,
        };
        // this.cardVehicleType = this.returnVehicleCode(this.cardInfo.vehicleType);
        // this.vehiCategoryExist = this.returnVehicleCategorycode(this.cardInfo.vehicleType)
        this.backgroundBtn = {
            "activatebackground" : false,
            "rechargebackground" : false,
            "updatebackground" :false,
        }
        this.recharge_amount = "";
        this.generic = {
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "lostCardActive" : true,
            "clearallDisable" :true,
            "rechargeInitited" : false,

            "updateInitited" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "doneDisable" : true,
        };
        if(this.readerMode == "Outer_Reader"){
            this.generic.rechargeDisable = true;
            this.generic.updateDisable = true;
        }
        environment.cardInfo = this.cardInfo;
        this.ref.detectChanges();
        calback();
    }
    //is card removed dal function for sdr10
    sdrCardRemoved() {
        console.log("sdr card removd method comes..")
        console.log('env sdr10 var in sdrCardRemoved',environment.sdrInitializeVariable)
        setTimeout(() => {
                this.dal.isCardRemoved(environment.sdrInitializeVariable, (resp) => {
                console.log("sdr card removd resp..",resp)
                if(resp.cardRemoved == true){
                    environment.cardInfo = {
                        passes : []
                    }
                    this.definecardDefaults();
                }
            })
        },120)
       
    }
    //is card removed dal function for buaroh
    buarohCardRemoved() {
        this.dal.isCardRemoved(environment.buarohinitializeVariable, (resp) => {
            if(resp.cardRemoved == true){
                this.definecardDefaults();
            }
        })
    }


    //recharge button action
    recharge() {  
        if(this.cardInfo.readerType == environment.sdrInitializeVariable.readerType){
            this.backgroundBtn = {
                "rechargebackground" : true,
            }
            this.generic ={
                "rechargeDisable" : false,
                "activateDisable" : true,
                "updateDisable" : true,
                "rechargeInitited" : true,
                "lostCardActive" : true,
                "rechargeamountDisable" : false,
                "vehiclenoActive" : true,
                "nameActive" : true,
                "mobilenoActive" : true,
                "cardnoActive" : true,
                "aadharnoActive" : true,
                "rfidActive" : true,
                "clearallDisable" :true,
            }
            
            this.ref.detectChanges(); 
        }
    }
    //update button
    update() {
      //  this.editAadharFlag = false;
        this.backgroundBtn = {
            "updatebackground" : true
        }
        this.generic = {
            "vehiclenoActive" : false,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "rechargeDisable" : true,
            "activateDisable" : true,
            "updateDisable" : false,
            "lostCardActive": true,
            "updateInitited" : true,
            "rechargeamountDisable" : true,
            "clearallDisable" :true,
            "doneDisable" : false,
        };
        this.ref.detectChanges();
    }
    //activate button
    activate() {
        this.backgroundBtn = {
            "activatebackground" : true,
        }
        this.generic = {
            "activateDisable" : false,
            "rechargeDisable" : true,
            "updateDisable" : true,
            "rechargeamountDisable" : true,
            "lostCardActive": true,
            "activateInitited" : true,
            "doneDisable" : false,
        };
        this.ref.detectChanges();
    }
    //check before activation
    checkBeforeActiveCard(){
        if(this.holderInfo.holderName && this.holderInfo.phoneNo && this.holderInfo.vehicleNo && this.holderInfo.cardNo && this.holderInfo.aadharNo && this.selectVehicle.vehicleType.label && this.selectVehicle.vehicleCat.category)
            return true;    
    }
    //after recharge action
    afterRecharge(){
        this.tempAction = true;
        this.backgroundBtn = {
            "rechargebackground" : false,
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "rechargeamountDisable" : true,
            "lostCardActive": false,
            "updateInitited" : false,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "vehiclenoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "clearallDisable" :true,
            "doneDisable" : true,
        };
        this.recharge_amount = '';
        this.donePopup('DONE');
       // this.cardActionResponse(this.popup);
        //this.ref.detectChanges();
    }

    //after update action
    afterUpdate(){
        this.backgroundBtn = {
            "updatebackground" :false
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "updateInitited" : false,
            "rechargeamountDisable" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "vehiclenoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "clearallDisable" :true,
            "doneDisable" : true,
        };
        //this.editAadharFlag = false;
        this.lostCard_number = "";
        //this.cardActionResponse(this.popup);
       // this.ref.detectChanges();
       this.donePopup('DONE');
    }

    //after activate action
    afterActivate(){
        this.backgroundBtn = {
            "activatebackground" : false,
        }
        this.generic = {
            "rechargeDisable" : false,
            "activateDisable" : true,
            "updateDisable" : false,
            "activateInitited" : false,
            "rechargeamountDisable" : true,
            "doneDisable" : true,
            "vehiclenoActive" : true,
            "nameActive" : true,
            "mobilenoActive" : true,
            "cardnoActive" : true,
            "aadharnoActive" : true,
            "rfidActive" : true,
            "lostCardActive" : false,
            "clearallDisable" :true,
        };
        this.lostCard_number = "";
      //  this.cardActionResponse(this.popup);
        this.donePopup('DONE');
        //this.ref.detectChanges();
    }
    //first tym activation action
    activateCardAction() {
        this.generic = {
            "rechargeDisable" : true,
            "activateDisable" : false,
            "updateDisable" : true,
        };
        this.finePopup.message = "PLEASE ACTIVATE THE CARD";
        this.finePopup.showPopup('WARNINGPOPUP');
        this.ref.detectChanges();    
    }
    //update action
    updateAction(){
        this.dal.stopDetection(environment.sdrInitializeVariable,(resultForUpdate)=> { 
            var updateParams = {
                "firstName": this.holderInfo.holderName,
                "name": this.holderInfo.holderName, 
                "middleName" : this.holderInfo.holderName,
                "lastName" : this.holderInfo.holderName,
                "gender": "MALE",
                "dateOfBirth": 1234567,
                "vehicleNumber" : this.holderInfo.vehicleNo,
                "mobile": parseInt(this.holderInfo.phoneNo),
                "aadhaar" : parseInt(this.holderInfo.aadharNo),
                "timeInMilliSeconds": "1578632680878",
                "readerType" :environment.sdrInitializeVariable.readerType,
                "readerIndex": 0
            }
            var updateApiParams = {
                "cardNumber" : this.holderInfo.cardNo,
                "vehicleNumber" : this.holderInfo.vehicleNo,
                "vehicleType" : this.cardInfo.vehicleType
            }
            setTimeout(() => {
                this.dal.updatePersonalData(updateParams, (resp)=> {
                console.log("update",resp);
                // this.doeStoreUpdateapi();
                this.afterUpdate();
                //api cal for update card info
                // this.commonserve.updateCardApi(updateApiParams).subscribe(respActivate=>{
                //     console.log("update done Api sucess....................",respActivate)
                // },err=>{
                //     alert(err.error.message)
                //     console.log("error in update ",err);
                // });
                this.ref.detectChanges();
                this.sdrCardRemoved();
            });
            },120)
            
        });
    }
     //recharge action
    rechargeAction(){
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultsdr) => {
            console.log("stop det in done......", resultsdr);
            if (this.recharge_amount) {
                let rechargeParams = {
                    "serviceType":"TOLL",
                    "action":"PAY",
                    "posTerminalId":"1234",
                    "amount": parseInt(this.recharge_amount),
                    "sourceBranchId":"231",
                    "destinationBranchId":"12",
                    "timeInMilliSeconds": this.commonserve.getEpocTimestamp(),
                    "readerType":environment.sdrInitializeVariable.readerType,
                    "readerIndex":0
                }
                //rechargeAPI params
               var rechargeParamsApi = {
                    "mode": "TOLL",
                    "amount": {
                        "value" : parseInt(this.recharge_amount),
                    },
                    "action": "CARD_TOP_UP",
                    "readerId": environment.readerId,
                    "cardNumber": this.holderInfo.cardNo,
                    "activityTime" : this.commonserve.getIsoStringTime(),
               } 
                this.commonserve.getRechargeCheck(rechargeParamsApi).subscribe(response=>{
                    console.log("response of rech limitssss",response);
                    
                        this.dal.recharge(rechargeParams,(resRecharge)=>{
                            console.log(":recharge dal params",rechargeParams)
                            console.log(":recharge from reader",resRecharge)
                           // this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                           
                            //rechrage api
                            // this.commonserve.recharge(rechargeParamsApi).subscribe(response=>{
                            //   //  console.log("recharge params api...",this.checkRechargeparmas)
                            //     console.log("recharge done res",response);
                                var dt = new Date();
                                var date1 = dt.toISOString(); // current date nd time
                                this.holderInfo.currentAmount = resRecharge.amount;
                                this.afterRecharge();
                                this.ref.detectChanges();
                                this.sdrCardRemoved();
                            // },err=>{
                            //       console.log("error in recharge ",err);
                            //       alert(err.error.message);
                            //       this.sdrCardRemoved();
                            // });
                            //recharge api end
                           // this.sdrCardRemoved();
                        })
                    
                },err=>{
                      console.log("u have crossed 20000 limit",err.error.message);
                      alert(err.error.message);
                      this.sdrCardRemoved();

                }); 
                //end
            }else{
               this.finePopup.message = "Please enter the amount";
               this.finePopup.showPopup('WARNINGPOPUP');
               this.sdrCardRemoved();
            }
        })
    }
     //activate action
    activateAction(){
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge)=>{
            //params for dal activate card
            if(this.checkBeforeActiveCard() == true)
            {
                var activateparams = {
                    "serviceType" : "TOLL",
                    "action" : "PAY",
                    "posTerminalId" : "1234",
                    "amount" : 100,
                    "sourceBranchId" : "231",
                    "destinationBranchId" : "12",
                    "timeInMilliSeconds" : "1578632680878",
                    "readerType" : environment.sdrInitializeVariable.readerType,
                    "readerIndex" : 0,
                    "cardNumber" : this.holderInfo.cardNo,
                    "name" : this.holderInfo.holderName,
                    "mobile" : parseInt(this.holderInfo.phoneNo),
                    "aadhaar" : parseInt(this.holderInfo.aadharNo),
                    "vehicleType": "NON",
                    "vehicleNumber" : this.holderInfo.vehicleNo,
                    "cardStatus" : "ACTIVATE"
                }
                    this.dal.activateCard(activateparams, (res)=> {
                        console.log("activate done....................",res)
                        this.afterActivate();
                        this.sdrCardRemoved();
                        this.ref.detectChanges();
                    });
                // },err=>{  
                //     alert(err.error.message)
                //     console.log("error in Activate ",err);
                //     this.sdrCardRemoved();
                // });
            } else{
                // this.finePopup.message = "Enter all the field";
                // this.finePopup.showPopup('WARNINGPOPUP');
                let arr = [this.holderInfo.holderName, this.holderInfo.phoneNo , this.holderInfo.vehicleNo , this.holderInfo.cardNo , this.holderInfo.aadharNo , this.selectVehicle.vehicleType.label , this.selectVehicle.vehicleCat.category];
                console.log("empty arrrr", arr)
                let arrStrMsg = ["NAME"," MOBILE NUMBER"," VEHICLE NUMBER"," CARD NUMBER"," AADHAR NUMBER"," VEHICLE TYPE"," VEHICLE CATEGORY"];
                let msg = [];  
                let selectMsg = [];    
                // arr.forEach((ele, index) => {
                //     if(!ele){
                //         console.log("ele nt there,,,,,......",ele)
                //         console.log("index nt there,,,,,.....",index)
                        

                //     }
                // })
                 console.log("ele nt there,,,,,......",arr)
                // console.log("index nt there,,,,,.....",index)
                for(let i=0; i<arr.length; i++){
                    if(!arr[i])
                    {    
                       // msg.push(arrStrMsg[i]);
                       if(i < 5){
                          msg.push(arrStrMsg[i])
                       }else{
                          selectMsg.push(arrStrMsg[i])
                       }
                    }              
                }
                
                console.log("msgggg in array22222.....",msg)
                let emptyValueMessage = msg.join(",");
                let selectValuMessage = selectMsg.join(",");
                console.log("msg created",emptyValueMessage)
                if(msg.length){
                    this.finePopup.message = "PLEASE ENTER " + emptyValueMessage;
                }
                if(!msg.length && selectMsg.length){
                    this.finePopup.message = "PLEASE SELECT " + selectValuMessage;
                }

               if(msg.length && selectMsg.length){
                    this.finePopup.message = "PLEASE ENTER " + emptyValueMessage + " & SELECT " + selectValuMessage;
                }
                
                this.finePopup.showPopup('WARNINGPOPUP');
                this.sdrCardRemoved();
            }
        });  
    }
    //done button click action
    doneButtonClick(popup) {
        this.popup = popup;
        console.log("reader type in done",this.cardInfo.readerType)
        if(this.cardInfo.readerType == environment.sdrInitializeVariable.readerType){
             if (window.navigator.onLine) {
                if (this.generic.updateInitited == true) {
                    this.updateAction();
                }

                if (this.generic.activateInitited == true) {
                    this.activateAction();
                }
                if (this.generic.rechargeInitited == true) {
                    this.rechargeAction();
                } 
             }else{
                this.finePopup.message = "cannot recharge in offline mode";
                this.finePopup.showPopup('WARNINGPOPUP');
             }  
        }
    }
    //get getenric data from sdr10
    scsproGetGenericDataFromCard(){
        this.dal.getGenericData(environment.sdrInitializeVariable, (response)=>{
            if(response.error){
                console.log("SDR10 ERR",response)
                if(response.error.name == "APP_ERROR") {
                   console.log("app error sdr10")
                }else{ 
                     console.log("different error in sdr10",response.error.name)
                     this.stopallReadersAndDefineDefaults();
                 }
            }else{
                //STOP DETECTION FOR BUAROH
                console.log("sucess part of error check")
                this.dal.stopDetection(environment.buarohinitializeVariable, (buarohstop) => {
                    console.log("buaroh stop detection IN sdr10",buarohstop);
                });
                
                this.cardInfo = response;
                this.tempCardInfo = response;
                console.log("sdr10 generic data", this.cardInfo);
                if (this.lostCard_number) { 
                    //this.generic.activateDisable = false;
                    if (this.cardInfo.phoneno) {
                        alert('card is already activated');
                    } else {
                        this.holderInfo.cardNo = this.cardInfo.cardno;
                    }
                } else {
                    this.readerMode = "Inner_Reader";
                    if(response.cardStatus != 'ACTIVATE') {
                            
                        this.genericAction(() => {
                           this.activateCardAction();  
                        });   
                        this.sdrCardRemoved(); 
                    }else{
                        console.log("generic action async in sdr10 comes")
                        this.genericAction(() => {
                            this.sdrCardRemoved(); 
                        }); 
                    }
                    
                    
                }
                
            }

       });
    }
    //get getenric data from buaroh
    buarohGetGenericDataFromCard(){
        console.log("entered buaroh1 methodd")
        this.dal.getGenericData( environment.buarohinitializeVariable,(res)=>{
            this.payParams.timeInMilliSeconds = "1578632680878";
            if(res.error){
                console.log("buarohError1",res)
                if(res.error.name == "APP_ERROR") {

                   console.log("app error buaroh1")
                }else{
                    console.log("different error in buaroh1",res.error.name)
                    this.stopallReadersAndDefineDefaults();
                }
            }else{
                console.log("buaroh generic data",res)

                // STOP DETECTION FOR SDR10
                this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge) => {
                    console.log("sdr10 stop detection IN BUAROH",resultForRecharge);                
                });
                
                this.cardInfo = res;
                if(res.cardStatus != 'ACTIVATE') {
                    this.activateCardAction();
                    this.buarohCardRemoved();          
                }else{
                    if(this.payParams.amount < 0){
                        this.payParams.amount = 0;
                    }else{
                        this.payParams.amount = this.payParams.amount;
                    }
                    if(res.passes[0]==null||this.cardInfo.passes[0].passType == "LOCAL"){
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                                if(this.cardInfo.balance >= this.payParams.amount) {
                                    this.payParams.readerIndex = environment.buarohinitializeVariable.readerIndex;
                                        this.dal.pay(this.payParams,(resPay)=>{
                                            if(!resPay.error){
                                                this.readerMode = "Outer_Reader";
                                                this.genericAction(() => {
                                                    console.log("generic action done")
                                                }); 
                                                this.holderInfo.currentAmount = resPay.amount;
                                                this.ref.detectChanges();
                                                console.log("payment is deducted with no pass",resPay); 
                                                               
                                            }else{
                                                this.finePopup.tap_again_message = "Tap Card Again";
                                                this.donePopup('TAPAGAIN');   
                                            }
                                            this.buarohCardRemoved(); 
                                        }); 
                                }else{
                                    this.finePopup.message = "Insufficient Amount";
                                    this.finePopup.showPopup('WARNINGPOPUP');
                                    this.buarohCardRemoved();
                                }
                            
                        }else{
                            this.finePopup.tap_again_message = "No Internet Connection";
                            this.donePopup('TAPAGAIN');
                            this.buarohCardRemoved();   
                        }
                     
                }
            }
        }
        });
    }



    //call getGenericDataFromCard from card
    getGenericDataFromCard() { 
        setTimeout(() => {
            this.scsproGetGenericDataFromCard();
        },120)
        setTimeout(() => {
            this.buarohGetGenericDataFromCard();
        },120)
           
    }
    //aadhar card popup call
    otpFingerscan() {
        this.finePopup.otpFingerscan();
    }
    otpFingerConfirmed() {
        this.finePopup.otpFingerConfirmed();
    }
    otpFingerNotRecognized() {
        this.finePopup.otpFingerNotRecognized();
    }
    otpConfirmedFinger() {
        this.finePopup.otpFingerConfirmed();
    }
    optInputFinger() {
       this.finePopup.otpFingerConfirmed();
    }
    otpOrFinger() {
        this.finePopup.otporFingerPopup();
    }
   
}