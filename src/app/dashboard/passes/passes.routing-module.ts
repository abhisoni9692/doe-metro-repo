import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { DailyPassComponent}     from './daily-pass/daily-pass.component';
import { LocalPassComponent } from './local-pass/local-pass.component';
import { MonthlyPassComponent } from './monthly-pass/monthly-pass.component';
import { ReturnPassComponent } from './return-pass/return-pass.component';
import { SinglePassComponent } from './single-pass/single-pass.component';
import { RenewalPassComponent } from './renewal-pass/renewal-pass.component';

const routes: Routes = [
  { path: '', component: MonthlyPassComponent},
  { path: 'dailypass', component: DailyPassComponent},
  { path: 'localpass', component: LocalPassComponent},
  { path: 'monthlypass', component: MonthlyPassComponent},
  { path: 'returnpass', component: ReturnPassComponent},
  { path: 'singlepass', component: SinglePassComponent},
  { path: 'renewalpass', component: RenewalPassComponent},
]

@NgModule({
  imports: [ RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class PassesRoutingModule {}
