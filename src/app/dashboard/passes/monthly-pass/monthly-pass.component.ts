import { HttpModule } from '@angular/http';

import { Component, ViewChild, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonBaseService } from "./../../../services/common.service";
import { DalService } from "./../../../services/dal.service";
import { environment } from "./../../../../environments/environment";
import { PopupComponent } from '../../../common/popup/popup.component';
declare var require: any;
@Component({
  templateUrl: 'monthly-pass.component.html'
})
export class MonthlyPassComponent {
    currentDateTime : any;
    dataInEpoc : any;
    MaxLImit : any;
    monthsCount : any;
    buarohinitializeVariable : any;
    temp : any;

    //ViewChild for Fine Popup
    @ViewChild(PopupComponent) private finePopup : PopupComponent;
        
    //ViewChild for Fine Popup --end --
    constructor(
        private commonserve:CommonBaseService,
        private modalService: NgbModal,
        private ref: ChangeDetectorRef,
        private router : Router,
        private dal : DalService,
    ){
        this.monthsCount = [];
        this.getNumberofMonths(); 
	}

    // route in monthly pass
    setRouter(temp){
        switch(temp){
            case "Single Journey": 
               // this.router.navigate(['/dashboard/passes/singlepass']);   
               break;
            case "localpass": 
               this.router.navigate(['/dashboard/passes/localpass']);   
               break;
            case "Return Journey": 
              // this.router.navigate(['/dashboard/passes/returnpass']);   
               break;
            case "monthlypass": 
               this.router.navigate(['/dashboard/passes/monthlypass']);   
               break;
       }    
    }
    //get the number of months
    getNumberofMonths(){
        for(let i=0; i< environment.feeStructureLimits['limits'].length; i++){
            console.log("vehicle typin pass",environment.vehicleTypeFromCard)
            if(environment.vehicleTypeFromCard == environment.feeStructureLimits['limits'][i].categoryCode && environment.feeStructureLimits['limits'][i].activityCode == "MONTHLY_PASS")
            {  
                this.monthsCount.push(environment.feeStructureLimits['limits'][i].periodicity); 
                console.log("no of counts",this.monthsCount)

            }  
        }
    }
    //get time in epoc
    getEpocTimestamp(){
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    }
    getISOString(epocTime){
       var ISOString = new Date(epocTime).toISOString();
       return ISOString;
    }
    getAmountFromApi(){
        var passAmount;
        var vehicleTypePayparams = environment.vehicleTypeFromCard;
        console.log(vehicleTypePayparams)
        passAmount = parseInt(environment.feeStructure[vehicleTypePayparams].MONTHLY_PASS.value); 
        return passAmount;
    }
    getPasslimits() {
   //   for(let i=0;i<environment.feeStructureLimits['baseFees'].length;i++){
     //     if(environment.vehicleTypeFromCard == environment.feeStructureLimits['baseFees'][i].categoryCode && environment.feeStructureLimits['baseFees'][i].activityCode == "MONTHLY_PASS")
       //     {
                for(let j=0;j<environment.feeStructureLimits['limits'].length;j++){
                    if(environment.vehicleTypeFromCard == environment.feeStructureLimits['limits'][j].categoryCode){
                       
                        this.MaxLImit = environment.feeStructureLimits['limits'][j].counter; 
                        console.log("trip",this.MaxLImit)
                        return this.MaxLImit;   
                    }
                    
              //  }
             
           // }  
        }
    }
     //is card removed dal function for sdr10
    sdrCardRemoved() {
        this.dal.isCardRemoved(environment.sdrInitializeVariable, (resp) => {
            if(resp.cardRemoved == true){
                environment.cardInfo = {
                    passes : []
                }
            }
        })
    }
    //api call and dal method after successful pass activation
	confirmPassActivation() {
        console.log("pass amount",this.getAmountFromApi())
        var timeInSeconds = this.getEpocTimestamp()+environment.timeDuration.monthly;
        timeInSeconds = timeInSeconds/1000;
        timeInSeconds = parseInt(timeInSeconds.toString());
        var businessid = Object.keys(environment['businessLevelId']['businessProfile']);
        var passParams = {
            "readerType" : "SCSPRO_READER",
            "readerIndex" : 0,
            "serviceType" : "TOLL",
            "sourceBranchId" : "123456789",
            "destinationBranchId" : "123456789",
            "periodicity" : [{
                "limitPeriodicity" : "MONTHLY",
                "limitMaxCount": 10
            },
            {
                "limitPeriodicity" : "DAILY" ,
                "limitMaxCount" : 10
            }],
            "expiryDate" : timeInSeconds,
            "timeInMilliSeconds" : this.getEpocTimestamp(),
            "passType": "MONTHLY",
            "maxTrips" : this.getPasslimits(),
            "isRenewal" : false,
            "amount" : this.getAmountFromApi(),
        }
         var passActivateApiParams = {
            "cardNumber": environment.cardInfo['cardNumber'],
            "readerId": environment.readerId,
            "activityTime": this.getISOString(this.getEpocTimestamp()),
            "amount": {
                "value": this.getAmountFromApi()
            },
            "mode": "TOLL",
            "action": "MONTHLY_PASS_PURCHASE",
            "vehicleNumber": environment.cardInfo['vehicleNumber'],
            "categoryCode": environment.cardInfo['vehicleType'],
            "activityCode": "MONTHLY_PASS",
            "pass": {
                "fromBusinessLevelId": businessid[0],
                "toBusinessLevelId": businessid[0],
                "tripsPending":  this.getPasslimits(),
                "passType": "MONTHLY_PASS",
                "expiresTime": this.getISOString(timeInSeconds),
                "periodicityLimits": {
                    "DAILY": {
                        "tripsPending": 0
                    },
                    "MONTHLY": {
                        "tripsPending": 0
                    }
                }
            }
        }
         this.commonserve.activatePassApi(passActivateApiParams).subscribe(respActivate=>{
           console.log("activate pass done Api sucess....................",respActivate)    
            this.dal.stopDetection(environment.sdrInitializeVariable, (resultForRecharge) => {
                console.log("sdr10 stop detection IN passes",resultForRecharge);
                this.dal.activatePass(passParams, (resultsdr) => {
                    console.log("pass result",resultsdr)
                    if(resultsdr.error){
                        alert(resultsdr.error.name)
                    }else{
                        this.donePopup('DONE');
                      //  this.sdrCardRemoved();

                    }
                })
            });                       
        },err=>{  
            alert(err.error.name)
            console.log("error in Activate pass ",err);
           // this.sdrCardRemoved();
        });
        
	}
    //Successfully done popup
    donePopup(message){
        this.finePopup.showPopup(message);
        setTimeout(()=> {
            this.finePopup.closePopup();

        },1000);
    }
    //successfully done popup ends
}

