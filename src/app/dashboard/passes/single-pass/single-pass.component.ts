import { HttpModule } from '@angular/http';

import { Component, ViewChild, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";

import { NgbModal, ModalDismissReasons, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonBaseService } from "./../../../services/common.service";
import { environment } from "./../../../../environments/environment";
import { DalService } from "./../../../services/dal.service";
declare var require: any;


@Component({
  templateUrl: 'single-pass.component.html',
})
export class SinglePassComponent {
	passParams : any;
    sdrInitializeVariable : any;
    currentDateTime : any;
    dataInEpoc : any;
    constructor(
        private commonserve:CommonBaseService,
        private modalService: NgbModal,
        private ref: ChangeDetectorRef,
        private dal_binding : DalService,
    ){
    	this.currentDateTime = new Date();
    	this.dataInEpoc = this.currentDateTime.getTime()/1000;
    	this.passParams = {
    		"readerType":"SCSPRO_READER",
    		"readerIndex":0,
    		"serviceType":"TOLL",
    		"sourceBranchId":"10601718",
    		"destinationBranchId":"10510328",
    		"periodicity": [{
    			"limitPeriodicity": "MONTHLY",
    			"limitMaxCount": 15
    		},
    		{
				"limitPeriodicity" : "DAILY" ,
				"limitMaxCount":2
    		}],
    		"expiryDate": 1530785820,
    		"timeInMilliSeconds" : this.dataInEpoc,
    		"passType": "SINGLE",
    		"maxTrips" : 30,
    		"isRenewal":false,
    		"amount":100
    	}
    	this.sdrInitializeVariable = {
            "readerType":"SCSPRO_READER",
            "readerIndex":0,
            "serviceType": "TOLL",
            "branchId": "1060171"
        }

        this.dal_binding.stopDetection(this.sdrInitializeVariable, (resultForRecharge) => {
            console.log("sdr10 stop detection IN BUAROH",resultForRecharge);
        });	
		this.dal_binding.activatePass(this.passParams, (resultsdr) => {
			console.log("pass result",resultsdr)
			this.dal_binding.isCardRemoved(this.sdrInitializeVariable, (resp) => {
                if(resp.cardRemoved == true){
                    console.log("card removed from sdr10 in pass",resp);
                    
                }
            });
		})
	}
}


