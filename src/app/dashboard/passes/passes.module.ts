import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DailyPassComponent } from './daily-pass/daily-pass.component';
import { LocalPassComponent } from './local-pass/local-pass.component';
import { MonthlyPassComponent } from './monthly-pass/monthly-pass.component';
import { ReturnPassComponent } from './return-pass/return-pass.component';
import { SinglePassComponent } from './single-pass/single-pass.component';
import { RenewalPassComponent } from './renewal-pass/renewal-pass.component';

import { PassesRoutingModule } from './passes.routing-module';
import { CommonBaseModule } from '../../common/common.module';
@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        PassesRoutingModule,
        CommonBaseModule
    ],
    declarations: [
        DailyPassComponent,
        LocalPassComponent,
        MonthlyPassComponent,
        ReturnPassComponent,
        SinglePassComponent,
        RenewalPassComponent

    ],
    exports: []
})
export class PassesModule { }
