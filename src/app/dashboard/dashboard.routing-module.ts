import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';



const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'passes', loadChildren: 'app/dashboard/passes/passes.module#PassesModule' },
  { path: 'settlement', loadChildren: 'app/dashboard/settlement/settlement.module#SettlementModule' }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
