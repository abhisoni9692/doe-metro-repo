import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonBaseService } from "../../../services/common.service";
import { Base64Service } from '../../../services/base64.service';
import { DalService } from "../../../services/dal.service";
import { environment } from "../../../../environments/environment";

@Component({
  templateUrl: 'card-settlement.component.html',
})
export class CardSettlementComponent {
    tollInfo : any;
    companyDetails : any;
    operatorinfo : any;
	constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private route : Router,  private base64 : Base64Service, private dal : DalService)
    {
		this.tollInfo = environment.tollInfo;
        this.companyDetails = environment.companyInfo;
        this.operatorinfo = environment.operatorDetails;
        this.dal.stopDetection(environment.sdrInitializeVariable, (resultsdr) => {
            console.log("sdr10 stopDetection in beginning",resultsdr) 
            this.dal.stopDetection(environment.buarohinitializeVariable, (resultbuaroh) => {
                this.dal.stopDetection(environment.buarohinitializeVariable2, (resultbuaroh2) => {
                    
                    console.log(" buaroh2 stopDetection in beginning",resultbuaroh2);
                });
                console.log(" buaroh stopDetection in beginning",resultbuaroh);
            });
        });
	}
}

