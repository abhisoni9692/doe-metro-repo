import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CardSettlementComponent } from './card-settlement/card-settlement.component';
import { ProfileComponent } from './profile/profile.component';
import { RateCardSettlementComponent } from './rate-card-settlement/rate-card-settlement.component';
import { TransactionSettlementComponent } from './transaction-settlement/transaction-settlement.component';

import { PassSettlementComponent } from './pass-settlement/pass-settlement.component';
import { RechargeSettlementComponent } from './recharge-settlement/recharge-settlement.component';
import { SummarySettlementComponent } from './summary-settlement/summary-settlement.component';

import { SettlementRoutingModule } from './settlement.routing-module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        SettlementRoutingModule,
        NgbModule.forRoot()
    ],
    declarations: [
        CardSettlementComponent,
        ProfileComponent,
        RateCardSettlementComponent,
        TransactionSettlementComponent,
        PassSettlementComponent,
        RechargeSettlementComponent,
        SummarySettlementComponent
    ],
    exports: []
})
export class SettlementModule { }
