import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from "../../../../environments/environment";
import { CommonBaseService } from "../../../services/common.service";
import { DalService } from "../../../services/dal.service";
import { Base64Service } from '../../../services/base64.service';


@Component({
  templateUrl: 'transaction-settlement.component.html',
})
export class TransactionSettlementComponent {
	tollInfo : any;
	companyDetails : any;
	operatorinfo : any;
	transaction_logs : any;
	recharge_logs : any;
	currentDate : any;
	threeMonthPrior : any;
	toISOString : any;	
	startdate_model : any;
	startchange : any;
	endchange : any;
	pagination : any;
	totalItems : any;
	lastLogs : any;
	lastSelectDate : any;
	minDate : any;
	maxDate : any;
	totalRecharge : any;
	noItem : any;
	flag : any;
	noTransactionMesage : any;
	constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private route : Router,  private base64 : Base64Service){
		this.tollInfo = environment.tollInfo;
    	this.companyDetails = environment.companyInfo;
    	this.operatorinfo = environment.operatorDetails;
		this.transaction_logs = {
			activities : []
		};
		this.recharge_logs = {
			activities : []
		}
		this.pagination = {
			"pageno" : 1,
			"pagesize" : 10
		}
		this.flag = 0;
		this.lastLogs = 0;
		this.lastLogs = 0;
		this.getCurrentDate();
		this.getMaxMinDate();
    	this.getTodayTransactionLogs();
	}
	//restrict customer to select date with 6 months
	getMaxMinDate(){
		let date =  new Date();
		this.maxDate = {
			"year" : date.getFullYear(),
			"month" : date.getMonth()+1,
			"day" : date.getDate()
		}
		let priorDateInMilisecond = date.setMonth(date.getMonth() - 5);
		let priorDate = new Date(priorDateInMilisecond)
		this.minDate = {
			"year" : priorDate.getFullYear(),
			"month" : priorDate.getMonth(),
			"day" : priorDate.getDate()
		}
	}
	//ends
	//convert date of Tz format and show in UI
	getDate(item){
		var temp = String(item).split('T')
		var date = temp[0]
		return date;
	}
	//ends
	formatAMPM(date) {
		let d = new Date(date);
	 	let hours: string | number = d.getHours();
		let minutes: string | number = d.getMinutes();
		let ampm: string | number = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		let strTime: string | number = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}
	getCurrentDate(){
		let today = new Date();
		this.toISOString = today.toISOString();
		let dd = String(today.getDate()).padStart(2, '0');
		let nextDay = String(today.getDate() -1).padStart(2, '0');
		let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		let yyyy = today.getFullYear();
		this.currentDate = {
			"dd" : dd,
			"nextDay" : nextDay,
			"mm" : mm,
			"yyyy" : yyyy
		}

		today.setMonth(today.getMonth() - 3);
		var localdate = today.toLocaleDateString();
		var arrayStartDate = localdate.split('/');
		var startday = arrayStartDate[0].padStart(2, '0');
		var startmonth = arrayStartDate[1].padStart(2, '0');
		var startyear = arrayStartDate[2];	
		this.threeMonthPrior = {
			"mm" : startday,
			"dd" : startmonth,
			"yyyy" : startyear
		}
		console.log("three month before",this.threeMonthPrior )
	}
	
	nextItems(type){	
	  	if(type ==  'NEXT'){
		 	if(this.pagination.pageno < this.totalItems){
		 		this.pagination.pageno = this.pagination.pageno+1;
		 		if(this.lastLogs == 1)
		 			this.onDateEndSelect(this.lastSelectDate);
		 		else
		 			this.getTodayTransactionLogs();
		 	}
	 	}else{
	 		if(this.pagination.pageno>1){
	 			this.pagination.pageno = this.pagination.pageno - 1;
	 			if(this.lastLogs == 1)
		 			this.onDateEndSelect(this.lastSelectDate);
		 		else
		 			this.getTodayTransactionLogs();
	 		}
	 	}

	}
	//ends
	//get logs for selected dates
	onDateEndSelect(endchange) {
		if(!this.startchange){
			alert("Please enter Start Date")
		}else{
			this.lastSelectDate = endchange;  
			this.lastLogs = 1;
			console.log("end date" , endchange)
			let today = new Date();
			let ISOString = today.toISOString();
			let todayDate = this.getDate(ISOString)
			let endchangeTemp = String(endchange.year).padStart(2, '0') + "-" + String(endchange.month).padStart(2, '0') + "-" + String(endchange.day).padStart(2, '0');
			if(endchangeTemp == todayDate){
				this.endchange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day + 1).padStart(2, '0')
				}
			}else{
				this.endchange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day).padStart(2, '0')
				}
			}
			
			let tempEndChange = this.endchange;
			tempEndChange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day + 1).padStart(2, '0')
				}
			console.log("this.endchange",tempEndChange)
			if(JSON.stringify(this.endchange) !== JSON.stringify(tempEndChange) && this.flag == 1)
			{

				this.pagination.pageno = 1
			}
			let queryparam = "?time_slice=lastthreemonths&start_time="+ this.startchange.year+"-"+this.startchange.month+"-"+ this.startchange.day+"&end_time="+ this.endchange.year+"-"+ this.endchange.month+"-"+ this.endchange.day + "&page_size=10&action=TOLL_ENTRY"+"&page="+this.pagination.pageno;

		    this.commonserve.transactionLogsApi(queryparam)
		    .subscribe(response =>{
		    	if(response['totalPages'] == 0 ){
		    		this.noTransactionMesage = "No Transaction";
	    			
	    		}else{
		    		this.noTransactionMesage = "";
		    		this.transaction_logs = response;
			    	console.log("recharge log working......",this.recharge_logs.activities)
			    	this.totalItems = this.transaction_logs['totalPages'];
		    	}
			})
		}
		
	}
	//to get start date from ngbdatepicker
	onDateStartSelect(startchange) {  
		console.log("start date", startchange)
		//this.startchange = startchange;
		if(JSON.stringify(this.startchange) == JSON.stringify(startchange)){
		}else{
			this.pagination.pageno = 1;
		}
		this.startchange = {
			"year" : String(startchange.year).padStart(2, '0'),
			"month" : String(startchange.month).padStart(2, '0'),
			"day" : String(startchange.day).padStart(2, '0')
		}
	}
	//ends
	//get today transaction logs
	getTodayTransactionLogs(){
		let queryparam = "?time_slice=lastthreemonths&start_time="+ this.currentDate.yyyy+"-"+this.currentDate.mm+"-"+ this.currentDate.dd+"&end_time="+this.toISOString + "&page_size="+this.pagination.pagesize+"&action=TOLL_ENTRY" +"&page="+this.pagination.pageno;
	    this.commonserve.transactionLogsApi(queryparam)
	    .subscribe(response =>{
	    	if(response['totalPages'] == 0 ){
	    		this.noTransactionMesage = "No Transaction";
    			
    		}else{
	    		this.noTransactionMesage = "";
	    		this.transaction_logs = response;
		    	console.log("recharge log working......",this.recharge_logs.activities)
		    	this.totalItems = this.transaction_logs['totalPages'];
	    	}
		})
	}
	//ends
}
