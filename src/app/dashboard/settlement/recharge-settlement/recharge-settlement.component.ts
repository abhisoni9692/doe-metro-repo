import { Component, Input, OnChanges, SimpleChanges  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonBaseService } from "../../../services/common.service";
import { Base64Service } from '../../../services/base64.service';
import { DalService } from "../../../services/dal.service";
import { environment } from "../../../../environments/environment";

@Component({
  templateUrl: 'recharge-settlement.component.html',
})
export class RechargeSettlementComponent {
	tollInfo : any;
	companyDetails : any;
	operatorinfo : any;
	transaction_logs : any;
	recharge_logs : any;
	currentDate : any;
	threeMonthPrior : any;
	toISOString : any;	
	startdate_model : any;
	startchange : any;
	endchange : any;
	pagination : any;
	totalItems : any;
	lastLogs : any;
	lastSelectDate : any;
	minDate : any;
	maxDate : any;
	totalRecharge : any;
	noItem : any;
	flag : any;
	noTransactionMesage : any;
	constructor(private commonserve : CommonBaseService, private activateroute: ActivatedRoute, private route : Router,  private base64 : Base64Service){
		this.tollInfo = environment.tollInfo;
    	this.companyDetails = environment.companyInfo;
    	this.operatorinfo = environment.operatorDetails;
		this.transaction_logs = {
			activities : []
		};
		this.recharge_logs = {
			activities : []
		}
		this.pagination = {
			"pageno" : 1,
			"pagesize" : 10
		}
		this.flag = 0;
		this.lastLogs = 0;
		this.getMaxMinDate();
		this.getCurrentDate();
    	this.getTodayRechargeLogs();
	}
	onDateStartSelect(startchange) {
		console.log("startchange chnge......jjjj",this.startchange)  
		console.log("start date", startchange)
		if(JSON.stringify(this.startchange) == JSON.stringify(startchange)){
		}else{
			this.pagination.pageno = 1;
		}
		this.startchange = {
			"year" : String(startchange.year).padStart(2, '0'),
			"month" : String(startchange.month).padStart(2, '0'),
			"day" : String(startchange.day).padStart(2, '0')
		}
	}
	getMaxMinDate(){

		let date =  new Date();
		this.maxDate = {
			"year" : date.getFullYear(),
			"month" : date.getMonth()+1,
			"day" : date.getDate()
		}
		let priorDateInMilisecond = date.setMonth(date.getMonth() - 5);
		let priorDate = new Date(priorDateInMilisecond)
		this.minDate = {
			"year" : priorDate.getFullYear(),
			"month" : priorDate.getMonth(),
			"day" : priorDate.getDate()
		}
	}
	onDateEndSelect(endchange) {
		if(!this.startchange){
			alert("Please enter Start Date")
		}else{
			this.lastSelectDate = endchange;  
			this.lastLogs = 1;
			let today = new Date();
			let ISOString = today.toISOString();
			let todayDate = this.getDate(ISOString)
			let endchangeTemp = String(endchange.year).padStart(2, '0') + "-" + String(endchange.month).padStart(2, '0') + "-" + String(endchange.day).padStart(2, '0');
			if(endchangeTemp == todayDate){
				this.endchange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day + 1).padStart(2, '0')
				}
			}else{
				this.endchange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day).padStart(2, '0')
				}
			}
			let tempEndChange = this.endchange;
			tempEndChange = {
					"year" : String(endchange.year).padStart(2, '0'),
					"month" : String(endchange.month).padStart(2, '0'),
					"day" : String(endchange.day + 1).padStart(2, '0')
				}
			console.log("this.endchange",tempEndChange)
			if(JSON.stringify(this.endchange) !== JSON.stringify(tempEndChange) && this.flag == 1)
			{

				this.pagination.pageno = 1
			}
			let queryparam = "?time_slice=lastthreemonths&start_time="+ this.startchange.year+"-"+this.startchange.month+"-"+ this.startchange.day+"&end_time="+ this.endchange.year+"-"+ this.endchange.month+"-"+ this.endchange.day + "&page_size=10&action=CARD_TOP_UP"+"&page="+this.pagination.pageno ;

		    this.commonserve.transactionLogsApi(queryparam)
		    .subscribe(response =>{
		    	console.log("recharge log working with date chnge......",response)
		    	if(response['totalItems'] == 0)
		    	{
		    		this.noTransactionMesage = "No Transaction";
		    	}
		    	else{
		    		this.noTransactionMesage = "";
		    		this.recharge_logs = response;
			    	console.log("recharge log working......",this.recharge_logs.activities)
			    	this.totalItems = this.recharge_logs['totalPages'];
		    	}
		    	
		    	
			})
		}
		
		
	}
	//convert date of Tz format and show in UI
	getDate(item){
		var temp = String(item).split('T')
		var date = temp[0]
		return date;
	}
	//ends
	//restrict customer to select date with 6 months
	getCurrentDate(){
		let today = new Date();
		this.toISOString = today.toISOString();
		let dd = String(today.getDate()).padStart(2, '0');
		let nextDay = String(today.getDate() -1).padStart(2, '0');
		let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		let yyyy = today.getFullYear();
		this.currentDate = {
			"dd" : dd,
			"nextDay" : nextDay,
			"mm" : mm,
			"yyyy" : yyyy
		}

		today.setMonth(today.getMonth() - 3);
		var localdate = today.toLocaleDateString();
		var arrayStartDate = localdate.split('/');
		var startday = arrayStartDate[0].padStart(2, '0');
		var startmonth = arrayStartDate[1].padStart(2, '0');
		var startyear = arrayStartDate[2];	
		this.threeMonthPrior = {
			"mm" : startday,
			"dd" : startmonth,
			"yyyy" : startyear
		}
		console.log("three month before",this.threeMonthPrior )
	}
	//get recharge transaction logs
	getTodayRechargeLogs(){
		let queryparam = "?time_slice=lastthreemonths&start_time="+ this.currentDate.yyyy+"-"+this.currentDate.mm+"-"+ this.currentDate.dd+"&end_time="+this.toISOString + "&page_size="+this.pagination.pagesize+"&action=CARD_TOP_UP" +"&page="+this.pagination.pageno;
	    this.commonserve.transactionLogsApi(queryparam)
	    .subscribe(response =>{
	    	if(response['totalPages'] == 0 ){
	    		this.noTransactionMesage = "No Transaction";
    			
    		}else{
	    		this.noTransactionMesage = "";
	    		this.recharge_logs = response;
		    	console.log("recharge log working......",this.recharge_logs.activities)
		    	this.totalItems = this.recharge_logs['totalPages'];
	    	}
		})
	 }
	 //ends
	nextItems(type){	
	  	if(type ==  'NEXT'){
		 	if(this.pagination.pageno < this.totalItems){
		 		this.pagination.pageno = this.pagination.pageno+1;
		 		if(this.lastLogs == 1)
		 			this.onDateEndSelect(this.lastSelectDate);
		 		else
		 			this.getTodayRechargeLogs();
		 	}
	 	}else{
	 		if(this.pagination.pageno>1){
	 			this.pagination.pageno = this.pagination.pageno - 1;
	 			if(this.lastLogs == 1)
		 			this.onDateEndSelect(this.lastSelectDate);
		 		else
		 			this.getTodayRechargeLogs();
	 		}
	 	}

	}
	showUnSuccessfulRecharge(){
		this.recharge_logs = {
			activities : []
		}
		this.noTransactionMesage = "No Transaction";
	}
	
}

