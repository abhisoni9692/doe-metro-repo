import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InputMobileNo } from './login/inputMobileNumber.component';
import { InputNewPassword } from './login/inputNewPassword.component';
import { InputOtp } from './login/inputOtp.component';
import { AppRoutingModule } from './routes/app-routing.module';
import { DirectivesModule } from './directives/directive.module';
//import { PipesModule } from './pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { TokenInterceptor} from './services/token.interceptor';
import { CommonBaseService } from "./services/common.service";
import { Base64Service } from './services/base64.service';
import { DalService } from './services/dal.service';
import { CommonBaseModule } from './common/common.module';
@NgModule({
  
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DirectivesModule,
    CommonBaseModule,
   // PipesModule,
    NgbModule.forRoot()
  ],
  providers: [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
    },
    CommonBaseService,
    Base64Service,
    DalService
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    InputMobileNo,
    InputNewPassword,
    InputOtp
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
