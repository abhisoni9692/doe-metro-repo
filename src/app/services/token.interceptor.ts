import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { CommonBaseService } from './common.service';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public commonServ: CommonBaseService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('intercepted request ... ');

    if (request.url.includes('token') || request.url.includes('revoke') ){
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/x-www-form-urlencoded') });
    }

    const token: string = this.commonServ.getToken();
    request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });

    return next.handle(request);
  }
}
