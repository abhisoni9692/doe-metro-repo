import {NgModule}               from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import {DidisplayCardNumberPipe }     from './pipes';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,

    ],
    declarations: [
      DidisplayCardNumberPipe,
     
    ],
    exports: [
      DidisplayCardNumberPipe,
      
    ]
})
export class PipesModule {

}