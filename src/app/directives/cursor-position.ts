declare var require: any;
import {Directive, ElementRef, HostListener, Input, ChangeDetectorRef, EventEmitter} from '@angular/core';

@Directive({
  selector: '[cursor]',
})
export class CursorPosition {
  caretPos : any;

  constructor(private el : ElementRef,private ref : ChangeDetectorRef)
  {
  }
  @Input('amount')recharge_amount : any;


  @HostListener('click', ['$event']) onClick() {
    this.onClickFunction(Event);
  }
  onClickFunction(e){ 
    let txt = document.getElementById("rate-card--recharge__value");
    this.caretPos = txt['selectionStart'];
    let textAreaTxt = this.recharge_amount.toString();
    let len = parseInt(this.caretPos);
    this.recharge_amount = textAreaTxt.substring(0, len-1) + textAreaTxt.substring(len);
    var elemntHtml = document.getElementById("rate-card--recharge__value");
    console.log(elemntHtml);
    elemntHtml['value'] = this.recharge_amount;
    elemntHtml['selectionStart'] = this.recharge_amount.length;
    let event = new Event('input');
    elemntHtml.dispatchEvent(event);
  }
}

