import { NgModule }               from '@angular/core';
import { FormsModule }            from '@angular/forms';


import { CursorPosition }          from "./cursor-position";
import { UppercaseDirective }          from "./toUpperCase";
import { AutofocusDirective }          from "./autoFocus";

@NgModule({
    imports: [
        FormsModule
    ],
    declarations: [
       CursorPosition,
       UppercaseDirective,
       AutofocusDirective
       
    ],
    exports: [
	   CursorPosition,
     UppercaseDirective,
     AutofocusDirective
	   
    ]
})
export class DirectivesModule {
}