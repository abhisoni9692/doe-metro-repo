import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent }         from '../app.component';
import { LoginComponent } from '../login/login.component';
import { InputMobileNo } from '../login/inputMobileNumber.component';
import { InputNewPassword } from '../login/inputNewPassword.component';
import { InputOtp } from '../login/inputOtp.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: LoginComponent},
  {path: 'inputNewPassword', component: InputNewPassword},
  {path: 'inputMobileNo', component: InputMobileNo},
  {path: 'inputOtp', component: InputOtp},
  { path: 'dashboard', loadChildren: 'app/dashboard/dashboard.module#DashboardModule'}

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
   ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
