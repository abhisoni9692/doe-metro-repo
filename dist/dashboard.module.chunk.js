webpackJsonp(["dashboard.module"],{

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__ = __webpack_require__("./src/app/common/popup/popup.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DashboardComponent = (function () {
    function DashboardComponent(commonserve, ref, base64, router, dal) {
        this.commonserve = commonserve;
        this.ref = ref;
        this.base64 = base64;
        this.router = router;
        this.dal = dal;
        this.hide_First_Screen = false;
        this.hide_Second_Screen = true;
        this.vehicle_Cat_no = 1;
        this.tempCardInfo = {
            aadhaar: "",
            balance: "",
            cardNumber: "",
            name: "",
            mobile: "",
            vehicleNumber: "",
            vehicleType: "",
            passes: []
        };
        //buaroh vaiable
        this.payParams = {
            "serviceType": "TOLL",
            "action": "PAY",
            "posTerminalId": "123456789",
            "amount": 0,
            "sourceBranchId": "123456789",
            "destinationBranchId": "12",
            "timeInMilliSeconds": '1578632680878',
            "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable.readerType,
            "readerIndex": 0
        };
        this.ownername = "";
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": true,
            "lostCardActive": false,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "activeVehicleClass": true,
        };
        this.recharge_amount = "";
        this.lostCard_number = "";
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        //use pass variable
        //stop all readers and define defaults and then intialize all
        this.stopallReadersAndDefineDefaults();
    }
    DashboardComponent.prototype.stopallReadersAndDefineDefaults = function () {
        var _this = this;
        console.log("comes in stopallReadersAndDefineDefaults method");
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultsdr) {
            console.log("sdr10 stopDetection in beginning in sdr10 stopallReadersAndDefineDefaults", resultsdr);
            _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resultbuaroh) {
                console.log("sdr10 stopDetection in beginning in buaroh1 stopallReadersAndDefineDefaults", resultbuaroh);
                _this.definecardDefaults();
            });
        });
    };
    //restrict character or number function ends
    DashboardComponent.prototype.onlyNumberKey = function (event) {
        this.editAadharFlag = true;
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    };
    DashboardComponent.prototype.restrictNumeric = function (e) {
        return (e.charCode == 8 || e.charCode == 0 || e.charCode == 32) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90;
    };
    DashboardComponent.prototype.restrictSpecialcharachter = function (e) {
        return (e.charCode == 8 || e.charCode == 0) ? null : e.charCode >= 97 && e.charCode <= 122 || e.charCode >= 65 && e.charCode <= 90 || e.charCode >= 48 && e.charCode <= 57;
    };
    //restrict character or number function ends
    DashboardComponent.prototype.enterFromKeyboard = function (e, popup) {
        if (e.charCode == 13) {
            this.doneButtonClick(popup);
            this.validClick = 1;
        }
    };
    DashboardComponent.prototype.numberButtonClick = function (rate, type) {
        if (this.validClick !== 1) {
            if (this.recharge_amount.toString().length < 6) {
                var temp = this.recharge_amount + rate;
                this.recharge_amount = parseInt(temp);
                this.digitBackgroundVar = true;
            }
        }
        this.validClick = 0;
    };
    ;
    DashboardComponent.prototype.rateButtonClick = function (rate, type) {
        this.recharge_amount = rate;
    };
    ;
    DashboardComponent.prototype.previousScreen = function () {
        this.hide_Second_Screen = true;
        this.hide_First_Screen = false;
        this.vehicle_Cat_no = 1;
        this.totalScreen = 1;
    };
    DashboardComponent.prototype.nextScreen = function () {
        this.hide_Second_Screen = false;
        this.hide_First_Screen = true;
        this.vehicle_Cat_no = 2;
        this.totalScreen = 2;
    };
    //changeaadhar mode
    DashboardComponent.prototype.changeAadharMode = function (flag) {
        this.editAadharFlag = flag;
        this.ref.detectChanges();
    };
    //clear all button action
    DashboardComponent.prototype.clearAll = function () {
        this.holderInfo = {
            "vehicleNo": "",
            "cardNo": "",
            "holderName": "",
            "phoneNo": "",
            "aadharNo": "",
            "rfidNo": "",
            "currentAmount": "",
        };
        this.lostCard_number = "";
        this.ref.detectChanges();
        this.getGenericDataFromCard();
    };
    //Successfully done popup
    DashboardComponent.prototype.donePopup = function (message) {
        var _this = this;
        this.finePopup.showPopup(message);
        setTimeout(function () {
            _this.finePopup.closePopup();
        }, 1000);
    };
    //successfully done popup ends
    //card function starts to call defualt card display
    DashboardComponent.prototype.definecardDefaults = function () {
        console.log("setting all the date in ui");
        var currentUrl = this.router.url;
        if (currentUrl == '/dashboard') {
            this.payParams.amount = "";
            this.cardInfo = "";
            this.holderInfo = {
                "vehicleNo": "",
                "cardNo": "",
                "holderName": "",
                "phoneNo": "",
                "aadharNo": "",
                "rfidNo": "",
                "currentAmount": "",
            };
            this.lostCard_number = "";
            this.generic = {
                "rechargeInitited": false,
                "updateInitited": false,
                "activateInitited": false,
                "rechargeDisable": true,
                "activateDisable": true,
                "updateDisable": true,
                "rechargeamountDisable": true,
                "doneDisable": true,
                "vehiclenoActive": true,
                "nameActive": true,
                "mobilenoActive": true,
                "cardnoActive": true,
                "aadharnoActive": true,
                "rfidActive": true,
                "lostCardActive": false,
                "clearallDisable": true,
            };
            this.recharge_amount = "";
            this.backgroundBtn = {
                "activatebackground": false,
                "rechargebackground": false,
                "updatebackground": false,
            };
            this.ref.detectChanges();
            this.getGenericDataFromCard();
        }
    };
    //display generic data function
    DashboardComponent.prototype.genericAction = function (calback) {
        this.editAadharFlag = false;
        this.holderInfo = {
            "vehicleNo": this.cardInfo.vehicleNumber,
            "cardNo": this.cardInfo.cardNumber,
            "holderName": this.cardInfo.name,
            "phoneNo": this.cardInfo.mobile,
            "aadharNo": this.cardInfo.aadhaar,
            "rfidNo": "",
            "currentAmount": this.cardInfo.balance,
        };
        // this.cardVehicleType = this.returnVehicleCode(this.cardInfo.vehicleType);
        // this.vehiCategoryExist = this.returnVehicleCategorycode(this.cardInfo.vehicleType)
        this.backgroundBtn = {
            "activatebackground": false,
            "rechargebackground": false,
            "updatebackground": false,
        };
        this.recharge_amount = "";
        this.generic = {
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "clearallDisable": true,
            "rechargeInitited": false,
            "updateInitited": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
        };
        if (this.readerMode == "Outer_Reader") {
            this.generic.rechargeDisable = true;
            this.generic.updateDisable = true;
        }
        __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].cardInfo = this.cardInfo;
        this.ref.detectChanges();
        calback();
    };
    //is card removed dal function for sdr10
    DashboardComponent.prototype.sdrCardRemoved = function () {
        var _this = this;
        console.log("sdr card removd method comes..");
        console.log('env sdr10 var in sdrCardRemoved', __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable);
        setTimeout(function () {
            _this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resp) {
                console.log("sdr card removd resp..", resp);
                if (resp.cardRemoved == true) {
                    __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].cardInfo = {
                        passes: []
                    };
                    _this.definecardDefaults();
                }
            });
        }, 120);
    };
    //is card removed dal function for buaroh
    DashboardComponent.prototype.buarohCardRemoved = function () {
        var _this = this;
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resp) {
            if (resp.cardRemoved == true) {
                _this.definecardDefaults();
            }
        });
    };
    //recharge button action
    DashboardComponent.prototype.recharge = function () {
        if (this.cardInfo.readerType == __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType) {
            this.backgroundBtn = {
                "rechargebackground": true,
            };
            this.generic = {
                "rechargeDisable": false,
                "activateDisable": true,
                "updateDisable": true,
                "rechargeInitited": true,
                "lostCardActive": true,
                "rechargeamountDisable": false,
                "vehiclenoActive": true,
                "nameActive": true,
                "mobilenoActive": true,
                "cardnoActive": true,
                "aadharnoActive": true,
                "rfidActive": true,
                "clearallDisable": true,
            };
            this.ref.detectChanges();
        }
    };
    //update button
    DashboardComponent.prototype.update = function () {
        //  this.editAadharFlag = false;
        this.backgroundBtn = {
            "updatebackground": true
        };
        this.generic = {
            "vehiclenoActive": false,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "rechargeDisable": true,
            "activateDisable": true,
            "updateDisable": false,
            "lostCardActive": true,
            "updateInitited": true,
            "rechargeamountDisable": true,
            "clearallDisable": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //activate button
    DashboardComponent.prototype.activate = function () {
        this.backgroundBtn = {
            "activatebackground": true,
        };
        this.generic = {
            "activateDisable": false,
            "rechargeDisable": true,
            "updateDisable": true,
            "rechargeamountDisable": true,
            "lostCardActive": true,
            "activateInitited": true,
            "doneDisable": false,
        };
        this.ref.detectChanges();
    };
    //check before activation
    DashboardComponent.prototype.checkBeforeActiveCard = function () {
        if (this.holderInfo.holderName && this.holderInfo.phoneNo && this.holderInfo.vehicleNo && this.holderInfo.cardNo && this.holderInfo.aadharNo && this.selectVehicle.vehicleType.label && this.selectVehicle.vehicleCat.category)
            return true;
    };
    //after recharge action
    DashboardComponent.prototype.afterRecharge = function () {
        this.tempAction = true;
        this.backgroundBtn = {
            "rechargebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "rechargeamountDisable": true,
            "lostCardActive": false,
            "updateInitited": false,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        this.recharge_amount = '';
        this.donePopup('DONE');
        // this.cardActionResponse(this.popup);
        //this.ref.detectChanges();
    };
    //after update action
    DashboardComponent.prototype.afterUpdate = function () {
        this.backgroundBtn = {
            "updatebackground": false
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "updateInitited": false,
            "rechargeamountDisable": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "vehiclenoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "clearallDisable": true,
            "doneDisable": true,
        };
        //this.editAadharFlag = false;
        this.lostCard_number = "";
        //this.cardActionResponse(this.popup);
        // this.ref.detectChanges();
        this.donePopup('DONE');
    };
    //after activate action
    DashboardComponent.prototype.afterActivate = function () {
        this.backgroundBtn = {
            "activatebackground": false,
        };
        this.generic = {
            "rechargeDisable": false,
            "activateDisable": true,
            "updateDisable": false,
            "activateInitited": false,
            "rechargeamountDisable": true,
            "doneDisable": true,
            "vehiclenoActive": true,
            "nameActive": true,
            "mobilenoActive": true,
            "cardnoActive": true,
            "aadharnoActive": true,
            "rfidActive": true,
            "lostCardActive": false,
            "clearallDisable": true,
        };
        this.lostCard_number = "";
        //  this.cardActionResponse(this.popup);
        this.donePopup('DONE');
        //this.ref.detectChanges();
    };
    //first tym activation action
    DashboardComponent.prototype.activateCardAction = function () {
        this.generic = {
            "rechargeDisable": true,
            "activateDisable": false,
            "updateDisable": true,
        };
        this.finePopup.message = "PLEASE ACTIVATE THE CARD";
        this.finePopup.showPopup('WARNINGPOPUP');
        this.ref.detectChanges();
    };
    //update action
    DashboardComponent.prototype.updateAction = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForUpdate) {
            var updateParams = {
                "firstName": _this.holderInfo.holderName,
                "name": _this.holderInfo.holderName,
                "middleName": _this.holderInfo.holderName,
                "lastName": _this.holderInfo.holderName,
                "gender": "MALE",
                "dateOfBirth": 1234567,
                "vehicleNumber": _this.holderInfo.vehicleNo,
                "mobile": parseInt(_this.holderInfo.phoneNo),
                "aadhaar": parseInt(_this.holderInfo.aadharNo),
                "timeInMilliSeconds": "1578632680878",
                "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                "readerIndex": 0
            };
            var updateApiParams = {
                "cardNumber": _this.holderInfo.cardNo,
                "vehicleNumber": _this.holderInfo.vehicleNo,
                "vehicleType": _this.cardInfo.vehicleType
            };
            setTimeout(function () {
                _this.dal.updatePersonalData(updateParams, function (resp) {
                    console.log("update", resp);
                    // this.doeStoreUpdateapi();
                    _this.afterUpdate();
                    //api cal for update card info
                    // this.commonserve.updateCardApi(updateApiParams).subscribe(respActivate=>{
                    //     console.log("update done Api sucess....................",respActivate)
                    // },err=>{
                    //     alert(err.error.message)
                    //     console.log("error in update ",err);
                    // });
                    _this.ref.detectChanges();
                    _this.sdrCardRemoved();
                });
            }, 120);
        });
    };
    //recharge action
    DashboardComponent.prototype.rechargeAction = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultsdr) {
            console.log("stop det in done......", resultsdr);
            if (_this.recharge_amount) {
                var rechargeParams_1 = {
                    "serviceType": "TOLL",
                    "action": "PAY",
                    "posTerminalId": "1234",
                    "amount": parseInt(_this.recharge_amount),
                    "sourceBranchId": "231",
                    "destinationBranchId": "12",
                    "timeInMilliSeconds": _this.commonserve.getEpocTimestamp(),
                    "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                    "readerIndex": 0
                };
                //rechargeAPI params
                var rechargeParamsApi = {
                    "mode": "TOLL",
                    "amount": {
                        "value": parseInt(_this.recharge_amount),
                    },
                    "action": "CARD_TOP_UP",
                    "readerId": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].readerId,
                    "cardNumber": _this.holderInfo.cardNo,
                    "activityTime": _this.commonserve.getIsoStringTime(),
                };
                _this.commonserve.getRechargeCheck(rechargeParamsApi).subscribe(function (response) {
                    console.log("response of rech limitssss", response);
                    _this.dal.recharge(rechargeParams_1, function (resRecharge) {
                        console.log(":recharge dal params", rechargeParams_1);
                        console.log(":recharge from reader", resRecharge);
                        // this.checkRechargeparmas.amount.value = parseInt(this.recharge_amount);
                        //rechrage api
                        // this.commonserve.recharge(rechargeParamsApi).subscribe(response=>{
                        //   //  console.log("recharge params api...",this.checkRechargeparmas)
                        //     console.log("recharge done res",response);
                        var dt = new Date();
                        var date1 = dt.toISOString(); // current date nd time
                        _this.holderInfo.currentAmount = resRecharge.amount;
                        _this.afterRecharge();
                        _this.ref.detectChanges();
                        _this.sdrCardRemoved();
                        // },err=>{
                        //       console.log("error in recharge ",err);
                        //       alert(err.error.message);
                        //       this.sdrCardRemoved();
                        // });
                        //recharge api end
                        // this.sdrCardRemoved();
                    });
                }, function (err) {
                    console.log("u have crossed 20000 limit", err.error.message);
                    alert(err.error.message);
                    _this.sdrCardRemoved();
                });
                //end
            }
            else {
                _this.finePopup.message = "Please enter the amount";
                _this.finePopup.showPopup('WARNINGPOPUP');
                _this.sdrCardRemoved();
            }
        });
    };
    //activate action
    DashboardComponent.prototype.activateAction = function () {
        var _this = this;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
            //params for dal activate card
            if (_this.checkBeforeActiveCard() == true) {
                var activateparams = {
                    "serviceType": "TOLL",
                    "action": "PAY",
                    "posTerminalId": "1234",
                    "amount": 100,
                    "sourceBranchId": "231",
                    "destinationBranchId": "12",
                    "timeInMilliSeconds": "1578632680878",
                    "readerType": __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType,
                    "readerIndex": 0,
                    "cardNumber": _this.holderInfo.cardNo,
                    "name": _this.holderInfo.holderName,
                    "mobile": parseInt(_this.holderInfo.phoneNo),
                    "aadhaar": parseInt(_this.holderInfo.aadharNo),
                    "vehicleType": "NON",
                    "vehicleNumber": _this.holderInfo.vehicleNo,
                    "cardStatus": "ACTIVATE"
                };
                _this.dal.activateCard(activateparams, function (res) {
                    console.log("activate done....................", res);
                    _this.afterActivate();
                    _this.sdrCardRemoved();
                    _this.ref.detectChanges();
                });
                // },err=>{  
                //     alert(err.error.message)
                //     console.log("error in Activate ",err);
                //     this.sdrCardRemoved();
                // });
            }
            else {
                // this.finePopup.message = "Enter all the field";
                // this.finePopup.showPopup('WARNINGPOPUP');
                var arr = [_this.holderInfo.holderName, _this.holderInfo.phoneNo, _this.holderInfo.vehicleNo, _this.holderInfo.cardNo, _this.holderInfo.aadharNo, _this.selectVehicle.vehicleType.label, _this.selectVehicle.vehicleCat.category];
                console.log("empty arrrr", arr);
                var arrStrMsg = ["NAME", " MOBILE NUMBER", " VEHICLE NUMBER", " CARD NUMBER", " AADHAR NUMBER", " VEHICLE TYPE", " VEHICLE CATEGORY"];
                var msg = [];
                var selectMsg = [];
                // arr.forEach((ele, index) => {
                //     if(!ele){
                //         console.log("ele nt there,,,,,......",ele)
                //         console.log("index nt there,,,,,.....",index)
                //     }
                // })
                console.log("ele nt there,,,,,......", arr);
                // console.log("index nt there,,,,,.....",index)
                for (var i = 0; i < arr.length; i++) {
                    if (!arr[i]) {
                        // msg.push(arrStrMsg[i]);
                        if (i < 5) {
                            msg.push(arrStrMsg[i]);
                        }
                        else {
                            selectMsg.push(arrStrMsg[i]);
                        }
                    }
                }
                console.log("msgggg in array22222.....", msg);
                var emptyValueMessage = msg.join(",");
                var selectValuMessage = selectMsg.join(",");
                console.log("msg created", emptyValueMessage);
                if (msg.length) {
                    _this.finePopup.message = "PLEASE ENTER " + emptyValueMessage;
                }
                if (!msg.length && selectMsg.length) {
                    _this.finePopup.message = "PLEASE SELECT " + selectValuMessage;
                }
                if (msg.length && selectMsg.length) {
                    _this.finePopup.message = "PLEASE ENTER " + emptyValueMessage + " & SELECT " + selectValuMessage;
                }
                _this.finePopup.showPopup('WARNINGPOPUP');
                _this.sdrCardRemoved();
            }
        });
    };
    //done button click action
    DashboardComponent.prototype.doneButtonClick = function (popup) {
        this.popup = popup;
        console.log("reader type in done", this.cardInfo.readerType);
        if (this.cardInfo.readerType == __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable.readerType) {
            if (window.navigator.onLine) {
                if (this.generic.updateInitited == true) {
                    this.updateAction();
                }
                if (this.generic.activateInitited == true) {
                    this.activateAction();
                }
                if (this.generic.rechargeInitited == true) {
                    this.rechargeAction();
                }
            }
            else {
                this.finePopup.message = "cannot recharge in offline mode";
                this.finePopup.showPopup('WARNINGPOPUP');
            }
        }
    };
    //get getenric data from sdr10
    DashboardComponent.prototype.scsproGetGenericDataFromCard = function () {
        var _this = this;
        this.dal.getGenericData(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (response) {
            if (response.error) {
                console.log("SDR10 ERR", response);
                if (response.error.name == "APP_ERROR") {
                    console.log("app error sdr10");
                }
                else {
                    console.log("different error in sdr10", response.error.name);
                    _this.stopallReadersAndDefineDefaults();
                }
            }
            else {
                //STOP DETECTION FOR BUAROH
                console.log("sucess part of error check");
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (buarohstop) {
                    console.log("buaroh stop detection IN sdr10", buarohstop);
                });
                _this.cardInfo = response;
                _this.tempCardInfo = response;
                console.log("sdr10 generic data", _this.cardInfo);
                if (_this.lostCard_number) {
                    //this.generic.activateDisable = false;
                    if (_this.cardInfo.phoneno) {
                        alert('card is already activated');
                    }
                    else {
                        _this.holderInfo.cardNo = _this.cardInfo.cardno;
                    }
                }
                else {
                    _this.readerMode = "Inner_Reader";
                    if (response.cardStatus != 'ACTIVATE') {
                        _this.genericAction(function () {
                            _this.activateCardAction();
                        });
                        _this.sdrCardRemoved();
                    }
                    else {
                        console.log("generic action async in sdr10 comes");
                        _this.genericAction(function () {
                            _this.sdrCardRemoved();
                        });
                    }
                }
            }
        });
    };
    //get getenric data from buaroh
    DashboardComponent.prototype.buarohGetGenericDataFromCard = function () {
        var _this = this;
        console.log("entered buaroh1 methodd");
        this.dal.getGenericData(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable, function (res) {
            _this.payParams.timeInMilliSeconds = "1578632680878";
            if (res.error) {
                console.log("buarohError1", res);
                if (res.error.name == "APP_ERROR") {
                    console.log("app error buaroh1");
                }
                else {
                    console.log("different error in buaroh1", res.error.name);
                    _this.stopallReadersAndDefineDefaults();
                }
            }
            else {
                console.log("buaroh generic data", res);
                // STOP DETECTION FOR SDR10
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
                    console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
                });
                _this.cardInfo = res;
                if (res.cardStatus != 'ACTIVATE') {
                    _this.activateCardAction();
                    _this.buarohCardRemoved();
                }
                else {
                    if (_this.payParams.amount < 0) {
                        _this.payParams.amount = 0;
                    }
                    else {
                        _this.payParams.amount = _this.payParams.amount;
                    }
                    if (res.passes[0] == null || _this.cardInfo.passes[0].passType == "LOCAL") {
                        //CAL SINGLE JOURNEY AND RETURN JOURNEY
                        //offline mode for demo
                        if (window.navigator.onLine) {
                            if (_this.cardInfo.balance >= _this.payParams.amount) {
                                _this.payParams.readerIndex = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].buarohinitializeVariable.readerIndex;
                                _this.dal.pay(_this.payParams, function (resPay) {
                                    if (!resPay.error) {
                                        _this.readerMode = "Outer_Reader";
                                        _this.genericAction(function () {
                                            console.log("generic action done");
                                        });
                                        _this.holderInfo.currentAmount = resPay.amount;
                                        _this.ref.detectChanges();
                                        console.log("payment is deducted with no pass", resPay);
                                    }
                                    else {
                                        _this.finePopup.tap_again_message = "Tap Card Again";
                                        _this.donePopup('TAPAGAIN');
                                    }
                                    _this.buarohCardRemoved();
                                });
                            }
                            else {
                                _this.finePopup.message = "Insufficient Amount";
                                _this.finePopup.showPopup('WARNINGPOPUP');
                                _this.buarohCardRemoved();
                            }
                        }
                        else {
                            _this.finePopup.tap_again_message = "No Internet Connection";
                            _this.donePopup('TAPAGAIN');
                            _this.buarohCardRemoved();
                        }
                    }
                }
            }
        });
    };
    //call getGenericDataFromCard from card
    DashboardComponent.prototype.getGenericDataFromCard = function () {
        var _this = this;
        setTimeout(function () {
            _this.scsproGetGenericDataFromCard();
        }, 120);
        setTimeout(function () {
            _this.buarohGetGenericDataFromCard();
        }, 120);
    };
    //aadhar card popup call
    DashboardComponent.prototype.otpFingerscan = function () {
        this.finePopup.otpFingerscan();
    };
    DashboardComponent.prototype.otpFingerConfirmed = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.otpFingerNotRecognized = function () {
        this.finePopup.otpFingerNotRecognized();
    };
    DashboardComponent.prototype.otpConfirmedFinger = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.optInputFinger = function () {
        this.finePopup.otpFingerConfirmed();
    };
    DashboardComponent.prototype.otpOrFinger = function () {
        this.finePopup.otporFingerPopup();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */])
    ], DashboardComponent.prototype, "finePopup", void 0);
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/dashboard.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_5__services_base64_service__["a" /* Base64Service */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_dal_service__["a" /* DalService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-div\">\r\n  <div class=\"dashboard-header\">\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SERVICE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>DOE PARKING</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>ZONE</span>\r\n        </div>\r\n        <div class=\"content-item text-background\">\r\n          <span>XXXXXXXX</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>PLAZA NAME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>NDTL TOLL</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LANE NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>7</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>787878</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>SUB SHIFT NO.</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>99</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\" style=\"border-right: 1px solid white;\">\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>LOGIC DATE TIME</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>12/03/2017</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"header-context\">\r\n        <div class=\"content-item\">\r\n          <span>TOLL COLLECTOR ID</span>\r\n        </div>\r\n        <div class=\"content-item\">\r\n          <span>1</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"header-section\">\r\n      <div class=\"reader-div\">\r\n        <div class=\"reader-status\">\r\n          <span>READER STATUS</span>\r\n        </div>\r\n        <div class=\"reader-status-button\">\r\n          <button type=\"\" class=\"button-green\"></button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"toll-dashboard\">\r\n    <div class=\" open-gate-section1 \" style=\"display:flex;flex-direction:column;\">\r\n      <div class=\"open-gate-section\" >\r\n        \r\n      </div>\r\n      <div class=\"overview\">\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__dashboard-meter\" id=\"datepicker\">\r\n          <img src=\"../src/images/dashboard-icon.png\" alt=\"\" />\r\n          <p>Dashboard</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\">\r\n          <img src=\"../src/images/system-view.png\" alt=\"\" />\r\n          <p>SYSTEM VIEW</p>\r\n        </button>\r\n        <button type=\"button\" name=\"button\" ngDefaultControl class=\"overview__settlements\" [routerLink]=\"['settlement']\">\r\n          <img src=\"../src/images/settlements-icon copy.png\" alt=\"\" />\r\n          <p>Settlements</p>\r\n        </button>\r\n        <div class=\"overview-lost-card\">\r\n          <div class=\"lost-card__data\">\r\n            <!-- <p>Lost card code</p> -->\r\n            <input type=\"text\" name=\"name\" class=\"lost-card__code\" value=\"\" placeholder=\"Lost card code\" [(ngModel)]=\"lostCard_number\" [readonly]=\"generic.lostCardActive\" (click)=\"lostCardTextbox()\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"toll-gate-form \">\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p>RFID</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" placeholder=\"*********\" [readonly]=\"generic.rfidActive\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">VEHICLE NO.</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"VEHICLE NO\" [(ngModel)]=\"holderInfo.vehicleNo\" [readonly]=\"generic.vehiclenoActive\" maxlength=\"10\"  (keypress)=\"restrictSpecialcharachter($event)\" uppercase>\r\n        </div>\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <img src=\"../src/images/no-1@2x.png\" class=\"car-number\" alt=\"\" />\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <img src=\"../src/images/no-2@2x.png\" class=\"car-number\" alt=\"\"  />\r\n        </div>\r\n      </div>\r\n      <!-- <div>\r\n        <img src=\"../src/images/no-1@2x.png\" class=\"car-number\" alt=\"\" />\r\n      </div> -->\r\n      <div class=\"data-feild\">\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">CARD NO.</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*********\" [(ngModel)]=\"holderInfo.cardNo\" [readonly]=\"generic.cardnoActive\" maxlength=\"16\" (keypress)=\"onlyNumberKey($event)\">\r\n        </div>\r\n        <div class=\"rfid-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">NAME</p>\r\n          </div>\r\n          <input type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"NAME\" [(ngModel)]=\"holderInfo.holderName\" [readonly]=\"generic.nameActive\" (keypress)=\"restrictNumeric($event)\">\r\n        </div>\r\n      </div>\r\n      <div class=\"data-feild\">\r\n        <div class=\"mobile-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">MOBILE NO.</p>\r\n          </div>\r\n          <input  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"*** *** ****\" size=\"10\" maxlength=\"10\" [(ngModel)]=\"holderInfo.phoneNo\" [readonly]=\"generic.mobilenoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"adhar-number\">\r\n          <div class=\"rfid-p-style\">\r\n            <p class=\"text-margin-bottom\">AADHAR NO.</p>\r\n          </div>  \r\n          <input *ngIf=\"!editAadharFlag\" (click)=\" changeAadharMode(true);\" type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [ngModel]=\"holderInfo.aadharNo | displayCardNumber\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n\r\n          <input *ngIf=\"editAadharFlag\"  type=\"text\" name=\"name\" class=\"rfid-number-value\" value=\"\" placeholder=\"**** **** ****\" [(ngModel)]=\"holderInfo.aadharNo\" maxlength=\"12\" [readonly]=\"generic.aadharnoActive\" (keypress)=\"onlyNumberKey($event)\" ngDefaultControl>\r\n        </div>\r\n        <div class=\"verify-button\">\r\n          <button (click)=\"otpFingerscan()\">VERIFY</button>\r\n        </div>\r\n      </div>\r\n      <div class=\"recharge-card-margin\">\r\n        <div class=\"recharge-balance-row\">\r\n          <div class=\"width-adjust\">\r\n            <button id=\"activate\" class=\"hide-outline \" (click)=\"recharge()\" [ngClass]=\"{'green-bckg': backgroundBtn.rechargebackground, 'blue-bckg':!backgroundBtn.rechargebackground}\" [disabled]=\"generic.rechargeDisable\">RECHARGE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"activate()\" [ngClass]=\"{'green-bckg': backgroundBtn.activatebackground, 'blue-bckg':!backgroundBtn.activatebackground}\" [disabled]=\"generic.activateDisable\">ACTIVATE</button>\r\n            <button id=\"recharge\" class=\"hide-outline\" (click)=\"update()\" [ngClass]=\"{'green-bckg': backgroundBtn.updatebackground, 'blue-bckg':!backgroundBtn.updatebackground}\" [disabled]=\"generic.updateDisable\">UPDATE</button>\r\n          </div>\r\n          <div class=\"width-fix\">\r\n            <div class=\"balance-card-amount\">\r\n              <p>Balance Amount</p>\r\n              <div class=\"amount-section\">\r\n                <p class=\"total-balance-p\" style=\"width:10%;\">&#8377;</p>\r\n                <div style=\"text-align: left;\">\r\n                  <input class=\"balance-input\" id=\"amount\" [(ngModel)]=\"holderInfo.currentAmount\" [ngClass]=\"amountStyle\" placeholder=\"10000\" maxlength=\"7\" readonly>\r\n                </div>\r\n                <!-- <p class=\"last-digit-p\" [ngClass]=\"lastDigitStyle\">00</p> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"clear-all-button\">\r\n              <button class=\"\" (click)=\"clearAll()\" [disabled]=\"generic.clearallDisable\">CLEAR ALL</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"toll-rate-cards\">\r\n          <div class=\"rate-card--view\">\r\n            <button type=\"button\" name=\"button\" ngDefaultControl class=\"rate-card__done-button\" (click)=\"doneButtonClick(cardactionResponse)\" [ngClass]=\"{'rate-card__done-button--active': !generic.doneDisable, 'rate-card__done-button--inactive':generic.doneDisable}\" [disabled]=\"generic.doneDisable\">DONE</button>\r\n            <div class=\"rate-card--recharge\">\r\n              <p>Recharge Amount</p>\r\n              <input type=\"text\" *ngIf=\"generic.rechargeamountDisable\"  name=\"name\" id=\"rate-card--recharge__value1\" class=\"rate-card--recharge__value\" maxlength=\"6\" value=\"\" placeholder=\"&#8377; 10000\" pattern=\"[0-9]\" [(ngModel)]=\"recharge_amount\" [disabled]=\"true\"  ngDefaultControl>\r\n              <input type=\"text\" *ngIf=\"!generic.rechargeamountDisable\" name=\"name\" id=\"rate-card--recharge__value\" class=\"rate-card--recharge__value\" maxlength=\"6\" value=\"\" placeholder=\"&#8377; 10000\" pattern=\"[0-9]\" [(ngModel)]=\"recharge_amount\" appAutofocus  [disabled]=\"false\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\"ngDefaultControl>\r\n            </div>\r\n            <div class=\"vehicles-rate-buttons\">\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('200',rate_button);\" value=\"200\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 200</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('300',rate_button);\" value=\"300\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 300</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('400',rate_button);\" value=\"400\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 400</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('500',rate_button);\" value=\"500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 500</button>\r\n              </div>\r\n              <div class=\"price-button-row\">\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('1500',rate_button);\" value=\"1500\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 1500</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('2000',rate_button);\" value=\"2000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 2000</button>\r\n                <button type=\"button\" class=\"rate-button\" (click)=\"rateButtonClick('5000',rate_button);\" value=\"5000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 5000</button>\r\n                <button type=\"button\" name=\"button\" id=\"rate-button1\" class=\"rate-button\" (click)=\"rateButtonClick('10000',rate_button);\" value=\"10000\" [disabled]=\"generic.rechargeamountDisable\" ngDefaultControl>&#8377; 10000</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"rate-card rate-card--values\">\r\n            <button type=\"button\" name=\"button\" class=\"\r\n            number-button\" (click)=\"numberButtonClick('7',rate_button);\" value=\"7\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>7</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('8',rate_button);\" value=\"8\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>8</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('9',rate_button);\" value=\"9\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>9</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('4',rate_button);\" value=\"4\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>4</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('5',rate_button);\" value=\"5\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>5</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('6',rate_button);\" value=\"6\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>6</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('1',rate_button);\" value=\"1\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>1</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('2',rate_button);\" value=\"2\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>2</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('3',rate_button);\" value=\"3\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>3</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('0',rate_button);\" value=\"0\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>0</button>\r\n            <button type=\"button\" name=\"button\" class=\"number-button\" (click)=\"numberButtonClick('00',rate_button);\" value=\"00\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" ngDefaultControl>00</button>\r\n            <button type=\"button\" name=\"button\" cursor [amount]=\"recharge_amount\" id=\"eraseNumber\" class=\"number-button\" [disabled]=\"generic.rechargeamountDisable\" (keypress)=\"enterFromKeyboard($event, cardactionResponse)\" [(ngModel)]=\"eraseButton\" ngDefaultControl><img src=\"../src/images/delete.png\" /></button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- Modal -->\r\n  <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"total-content\">\r\n      <div class=\"modal-body\">\r\n        <img class=\"wrong-vehicle-design\" src=\"../src/images/car.png\" alt=\"\" />\r\n      </div>\r\n      <div class=\"wrong-vehicle-text\">\r\n        ARE YOU SURE ?\r\n      </div>\r\n      <div class=\"button-yes-no\">\r\n        <button class=\"no-confirmation\" (click)=\"c('Close click')\">NO</button>\r\n        <button class=\"yes-confirmation\">YES</button>\r\n      </div>\r\n    </div>\r\n    \r\n  </ng-template>\r\n  \r\n\r\n<!-- Modal for card action popup-->\r\n<ng-template role=\"dialog\"   let-c=\"close\" let-d=\"dismiss\" #cardactionResponse >\r\n   <div class=\"modal-content-card\">\r\n      <div class=\"close-modal \">\r\n        <button>\r\n          <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n        </button>\r\n      </div>\r\n      <div class=\"message-button\">\r\n        <span>Successful</span>\r\n        <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n      </div>\r\n   </div>\r\n</ng-template>\r\n\r\n<!-- Modal for card action popup ends-->\r\n<div>\r\n  <popup (setDefault)=\"definecardDefaults()\"></popup>\r\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__ = __webpack_require__("./src/app/dashboard/dashboard.routing-module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directive_module__ = __webpack_require__("./src/app/directives/directive.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pipes_pipes_module__ = __webpack_require__("./src/app/pipes/pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__common_common_module__ = __webpack_require__("./src/app/common/common.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__common_common_module__["a" /* CommonBaseModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__dashboard_routing_module__["a" /* DashboardRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directive_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_7__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__dashboard_component__["a" /* DashboardComponent */]
            ],
            providers: [],
            exports: []
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.routing-module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__dashboard_component__["a" /* DashboardComponent */] },
    { path: 'passes', loadChildren: 'app/dashboard/passes/passes.module#PassesModule' },
    { path: 'settlement', loadChildren: 'app/dashboard/settlement/settlement.module#SettlementModule' }
];
var DashboardRoutingModule = (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=dashboard.module.chunk.js.map