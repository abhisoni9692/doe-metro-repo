webpackJsonp(["passes.module"],{

/***/ "./src/app/dashboard/passes/daily-pass/daily-pass.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" pass_types_main_section\">\r\n\t<div class=\"pass-option\">\r\n\t\t<a [routerLink]=\"['../dailypass']\">\r\n    \t\t<button class=\"active\">DAILY PASS</button>\r\n\t \t</a>\r\n\t\t<a [routerLink]=\"['../monthlypass']\">\r\n    \t\t<button>MONTHLY PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../localpass']\">\r\n    \t\t<button>LOCAL PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../renewalpass']\">\r\n    \t\t<button>RENEWALS</button>\r\n\t \t</a>\r\n\t</div>\r\n\t<div class= \"flavour-range-div\">\r\n\t\t<div class=\"pass-flavour monthly_pass_kilometer_range\">\r\n\t\t\t<div class=\"kilometer-range\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tONE MONTH\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tTWO MONTHS\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\r\n\t\t</div>\r\n\t\t<div class=\"second-part\">\r\n\t\t\t<div class=\"date-trip-time\">\r\n\t\t\t\t<div class=\"current-date\">\r\n\t\t\t\t\t<P>11-08-2017  /  04:11 PM</P>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"date-validity\">\r\n\t\t\t\t\t<p>Date Validity:  11/08/2017 - 04:11 PM(24 hrs)</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"trips-validity\">\r\n\t\t\t\t\t<p>Trips Validity: 5/Multi</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"total-text\">\r\n\t\t\t\t\t<span>Total Amount:</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"amount\">\r\n\t\t\t\t\t<span>&#8377;</span>\r\n\t\t\t\t\t<span>1500</span>\r\n\t\t\t\t\t<p>00</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"activate-pass\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\">ACTIVATE / DONE</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"back-button\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\" [routerLink]=\"['../../../dashboard']\">BACK</button>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<!-- Modal for card success popup-->\r\n <ng-template role=\"dialog\"   let-c=\"close\" let-d=\"dismiss\" #cardactionResponse >\r\n     <div class=\"modal-content-card\">\r\n        <div class=\"close-modal \">\r\n          <button>\r\n            <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n          </button>\r\n        </div>\r\n        <div class=\"message-button\">\r\n          <span>Successful</span>\r\n          <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n        </div>\r\n     </div>\r\n  </ng-template>\r\n\r\n  <!-- Modal for card success popup ends-->  "

/***/ }),

/***/ "./src/app/dashboard/passes/daily-pass/daily-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyPassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DailyPassComponent = (function () {
    function DailyPassComponent() {
    }
    DailyPassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/daily-pass/daily-pass.component.html"),
        })
    ], DailyPassComponent);
    return DailyPassComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/local-pass/local-pass.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" pass_types_main_section\">\r\n\t<div class=\"pass-option\">\r\n\t\t<a [routerLink]=\"['../dailypass']\">\r\n    \t\t<button>DAILY PASS</button>\r\n\t \t</a>\r\n\t\t<a [routerLink]=\"['../monthlypass']\">\r\n    \t\t<button>MONTHLY PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../localpass']\">\r\n    \t\t<button class=\"active\">LOCAL PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../renewalpass']\">\r\n    \t\t<button>RENEWALS</button>\r\n\t \t</a>\r\n\t</div>\r\n\t<div class= \"flavour-range-div\">\r\n\t\t<div class=\"pass-flavour\">\r\n\t\t\t<div class=\"kilometer-range\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tBELOW 10 KM\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tBELOW 20 KM\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"kilometer-range\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tONE MONTH\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tTWO MONTHS\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\r\n\t\t</div>\r\n\t\t<div class=\"second-part\">\r\n\t\t\t<div class=\"date-trip-time\">\r\n\t\t\t\t<div class=\"current-date\">\r\n\t\t\t\t\t<P>11-08-2017  /  04:11 PM</P>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"date-validity\">\r\n\t\t\t\t\t<p>Date Validity:  11/08/2017 - 10/09/2017</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"trips-validity\">\r\n\t\t\t\t\t<p>Trips Validity:  40 ( Daily / Monthly )</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"total-text\">\r\n\t\t\t\t\t<span>Total Amount:</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"amount\">\r\n\t\t\t\t\t<span>&#8377;</span>\r\n\t\t\t\t\t<span>1500</span>\r\n\t\t\t\t\t<p>00</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"activate-pass\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\" (click)=\"confirmPassActivation(cardactionResponse);\">ACTIVATE / DONE</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"back-button\">\r\n\t\t\t    \t<button class= \"pass__default_btn\" [routerLink]=\"['../../../dashboard']\">BACK</button>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<!-- Modal for card success popup-->\r\n <ng-template role=\"dialog\"   let-c=\"close\" let-d=\"dismiss\" #cardactionResponse >\r\n     <div class=\"modal-content-card\">\r\n        <div class=\"close-modal \">\r\n          <button>\r\n            <img src=\"../src/images/close.png\" (click)=\"c('Close click')\">\r\n          </button>\r\n        </div>\r\n        <div class=\"message-button\">\r\n          <span>Successful</span>\r\n          <button  (click)=\"c('Close click')\" autofocus>Done</button>\r\n        </div>\r\n     </div>\r\n  </ng-template>\r\n\r\n  <!-- Modal for card success popup ends-->  "

/***/ }),

/***/ "./src/app/dashboard/passes/local-pass/local-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalPassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LocalPassComponent = (function () {
    function LocalPassComponent(commonserve, modalService, ref, dal, router) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.dal = dal;
        this.router = router;
    }
    //get time in epoc
    LocalPassComponent.prototype.getEpocTimestamp = function () {
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    };
    LocalPassComponent.prototype.getISOString = function (epocTime) {
        var ISOString = new Date(epocTime).toISOString();
        return ISOString;
    };
    //is card removed dal function for sdr10
    LocalPassComponent.prototype.sdrCardRemoved = function () {
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resp) {
            if (resp.cardRemoved == true) {
                __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].cardInfo = {
                    passes: []
                };
            }
        });
    };
    LocalPassComponent.prototype.confirmPassActivation = function (popup) {
        var _this = this;
        var timeInSeconds = this.getEpocTimestamp() + __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].timeDuration.monthly;
        timeInSeconds = timeInSeconds / 1000;
        timeInSeconds = parseInt(timeInSeconds.toString());
        var passParams = {
            "readerType": "SCSPRO_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "sourceBranchId": "123456789",
            "destinationBranchId": "123456789",
            "periodicity": [{
                    "limitPeriodicity": "MONTHLY",
                    "limitMaxCount": 10
                },
                {
                    "limitPeriodicity": "DAILY",
                    "limitMaxCount": 10
                }],
            "expiryDate": timeInSeconds,
            "timeInMilliSeconds": this.getEpocTimestamp(),
            "passType": "LOCAL",
            "maxTrips": 0,
            "isRenewal": false,
            "amount": 0,
        };
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
            console.log("sdr10 stop detection IN passes", resultForRecharge);
            _this.dal.activatePass(passParams, function (resultsdr) {
                console.log("pass result", resultsdr);
                if (resultsdr.error) {
                    alert(resultsdr.error.message);
                }
                else {
                    alert("pass activated");
                    _this.sdrCardRemoved();
                }
            });
        });
    };
    LocalPassComponent.prototype.cardActionResponse = function (cardactionResponse) {
        var _this = this;
        this.modalService.open(cardactionResponse).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    LocalPassComponent.prototype.getDismissReason = function (reason) {
        if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    LocalPassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/local-pass/local-pass.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_5__services_dal_service__["a" /* DalService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], LocalPassComponent);
    return LocalPassComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/monthly-pass/monthly-pass.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" pass_types_main_section\">\r\n\t<div class=\"pass-option\">\r\n\t\t<a [routerLink]=\"['../dailypass']\">\r\n    \t\t<button >DAILY PASS</button>\r\n\t \t</a>\r\n\t\t<a [routerLink]=\"['../monthlypass']\">\r\n    \t\t<button class=\"active\">MONTHLY PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../localpass']\">\r\n    \t\t<button >LOCAL PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../renewalpass']\">\r\n    \t\t<button >RENEWALS</button>\r\n\t \t</a>\r\n\t</div>\r\n\t<div class= \"flavour-range-div\">\r\n\t\t<div class=\"pass-flavour monthly_pass_kilometer_range\">\r\n\t\t\t<div class=\"kilometer-range\" *ngIf=\"monthsCount.length\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\" *ngFor=\"let item of monthsCount\">\r\n\t\t\t\t\t{{item}}\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\r\n\t\t</div>\r\n\t\t<div class=\"second-part\">\r\n\t\t\t<div class=\"date-trip-time\">\r\n\t\t\t\t<div class=\"current-date\">\r\n\t\t\t\t\t<P>11-08-2018 /  04:11 PM</P>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"date-validity\">\r\n\t\t\t\t\t<p>Date Validity:  11/08/2017 - 10/09/2017</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"trips-validity\">\r\n\t\t\t\t\t<p></p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"total-text\">\r\n\t\t\t\t\t<span>Total Amount:</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"amount\">\r\n\t\t\t\t\t<span>&#8377;</span>\r\n\t\t\t\t\t<span></span>\r\n\t\t\t\t\t<p>00</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"activate-pass\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\" (click)=\"confirmPassActivation();\">ACTIVATE / DONE</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"back-button\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\" [routerLink]=\"['../../../dashboard']\">BACK</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n</div>\r\n<!-- Modal for card action popup ends-->\r\n<div>\r\n  <popup></popup>\r\n</div>"

/***/ }),

/***/ "./src/app/dashboard/passes/monthly-pass/monthly-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonthlyPassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__ = __webpack_require__("./src/app/common/popup/popup.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MonthlyPassComponent = (function () {
    //ViewChild for Fine Popup --end --
    function MonthlyPassComponent(commonserve, modalService, ref, router, dal) {
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.router = router;
        this.dal = dal;
        this.monthsCount = [];
        this.getNumberofMonths();
    }
    // route in monthly pass
    MonthlyPassComponent.prototype.setRouter = function (temp) {
        switch (temp) {
            case "Single Journey":
                // this.router.navigate(['/dashboard/passes/singlepass']);   
                break;
            case "localpass":
                this.router.navigate(['/dashboard/passes/localpass']);
                break;
            case "Return Journey":
                // this.router.navigate(['/dashboard/passes/returnpass']);   
                break;
            case "monthlypass":
                this.router.navigate(['/dashboard/passes/monthlypass']);
                break;
        }
    };
    //get the number of months
    MonthlyPassComponent.prototype.getNumberofMonths = function () {
        for (var i = 0; i < __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'].length; i++) {
            console.log("vehicle typin pass", __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].vehicleTypeFromCard);
            if (__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].vehicleTypeFromCard == __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'][i].categoryCode && __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'][i].activityCode == "MONTHLY_PASS") {
                this.monthsCount.push(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'][i].periodicity);
                console.log("no of counts", this.monthsCount);
            }
        }
    };
    //get time in epoc
    MonthlyPassComponent.prototype.getEpocTimestamp = function () {
        var currentDateTime = new Date();
        var dataInEpoc = currentDateTime.getTime();
        return dataInEpoc;
    };
    MonthlyPassComponent.prototype.getISOString = function (epocTime) {
        var ISOString = new Date(epocTime).toISOString();
        return ISOString;
    };
    MonthlyPassComponent.prototype.getAmountFromApi = function () {
        var passAmount;
        var vehicleTypePayparams = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].vehicleTypeFromCard;
        console.log(vehicleTypePayparams);
        passAmount = parseInt(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructure[vehicleTypePayparams].MONTHLY_PASS.value);
        return passAmount;
    };
    MonthlyPassComponent.prototype.getPasslimits = function () {
        //   for(let i=0;i<environment.feeStructureLimits['baseFees'].length;i++){
        //     if(environment.vehicleTypeFromCard == environment.feeStructureLimits['baseFees'][i].categoryCode && environment.feeStructureLimits['baseFees'][i].activityCode == "MONTHLY_PASS")
        //     {
        for (var j = 0; j < __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'].length; j++) {
            if (__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].vehicleTypeFromCard == __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'][j].categoryCode) {
                this.MaxLImit = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].feeStructureLimits['limits'][j].counter;
                console.log("trip", this.MaxLImit);
                return this.MaxLImit;
            }
            //  }
            // }  
        }
    };
    //is card removed dal function for sdr10
    MonthlyPassComponent.prototype.sdrCardRemoved = function () {
        this.dal.isCardRemoved(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resp) {
            if (resp.cardRemoved == true) {
                __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].cardInfo = {
                    passes: []
                };
            }
        });
    };
    //api call and dal method after successful pass activation
    MonthlyPassComponent.prototype.confirmPassActivation = function () {
        var _this = this;
        console.log("pass amount", this.getAmountFromApi());
        var timeInSeconds = this.getEpocTimestamp() + __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].timeDuration.monthly;
        timeInSeconds = timeInSeconds / 1000;
        timeInSeconds = parseInt(timeInSeconds.toString());
        var businessid = Object.keys(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */]['businessLevelId']['businessProfile']);
        var passParams = {
            "readerType": "SCSPRO_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "sourceBranchId": "123456789",
            "destinationBranchId": "123456789",
            "periodicity": [{
                    "limitPeriodicity": "MONTHLY",
                    "limitMaxCount": 10
                },
                {
                    "limitPeriodicity": "DAILY",
                    "limitMaxCount": 10
                }],
            "expiryDate": timeInSeconds,
            "timeInMilliSeconds": this.getEpocTimestamp(),
            "passType": "MONTHLY",
            "maxTrips": this.getPasslimits(),
            "isRenewal": false,
            "amount": this.getAmountFromApi(),
        };
        var passActivateApiParams = {
            "cardNumber": __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].cardInfo['cardNumber'],
            "readerId": __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].readerId,
            "activityTime": this.getISOString(this.getEpocTimestamp()),
            "amount": {
                "value": this.getAmountFromApi()
            },
            "mode": "TOLL",
            "action": "MONTHLY_PASS_PURCHASE",
            "vehicleNumber": __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].cardInfo['vehicleNumber'],
            "categoryCode": __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].cardInfo['vehicleType'],
            "activityCode": "MONTHLY_PASS",
            "pass": {
                "fromBusinessLevelId": businessid[0],
                "toBusinessLevelId": businessid[0],
                "tripsPending": this.getPasslimits(),
                "passType": "MONTHLY_PASS",
                "expiresTime": this.getISOString(timeInSeconds),
                "periodicityLimits": {
                    "DAILY": {
                        "tripsPending": 0
                    },
                    "MONTHLY": {
                        "tripsPending": 0
                    }
                }
            }
        };
        this.commonserve.activatePassApi(passActivateApiParams).subscribe(function (respActivate) {
            console.log("activate pass done Api sucess....................", respActivate);
            _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultForRecharge) {
                console.log("sdr10 stop detection IN passes", resultForRecharge);
                _this.dal.activatePass(passParams, function (resultsdr) {
                    console.log("pass result", resultsdr);
                    if (resultsdr.error) {
                        alert(resultsdr.error.name);
                    }
                    else {
                        _this.donePopup('DONE');
                        //  this.sdrCardRemoved();
                    }
                });
            });
        }, function (err) {
            alert(err.error.name);
            console.log("error in Activate pass ", err);
            // this.sdrCardRemoved();
        });
    };
    //Successfully done popup
    MonthlyPassComponent.prototype.donePopup = function (message) {
        var _this = this;
        this.finePopup.showPopup(message);
        setTimeout(function () {
            _this.finePopup.closePopup();
        }, 1000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__common_popup_popup_component__["a" /* PopupComponent */])
    ], MonthlyPassComponent.prototype, "finePopup", void 0);
    MonthlyPassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/monthly-pass/monthly-pass.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_dal_service__["a" /* DalService */]])
    ], MonthlyPassComponent);
    return MonthlyPassComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/passes.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassesModule", function() { return PassesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__daily_pass_daily_pass_component__ = __webpack_require__("./src/app/dashboard/passes/daily-pass/daily-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__local_pass_local_pass_component__ = __webpack_require__("./src/app/dashboard/passes/local-pass/local-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__monthly_pass_monthly_pass_component__ = __webpack_require__("./src/app/dashboard/passes/monthly-pass/monthly-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__return_pass_return_pass_component__ = __webpack_require__("./src/app/dashboard/passes/return-pass/return-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__single_pass_single_pass_component__ = __webpack_require__("./src/app/dashboard/passes/single-pass/single-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__renewal_pass_renewal_pass_component__ = __webpack_require__("./src/app/dashboard/passes/renewal-pass/renewal-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__passes_routing_module__ = __webpack_require__("./src/app/dashboard/passes/passes.routing-module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__common_common_module__ = __webpack_require__("./src/app/common/common.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var PassesModule = (function () {
    function PassesModule() {
    }
    PassesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_9__passes_routing_module__["a" /* PassesRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_10__common_common_module__["a" /* CommonBaseModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__daily_pass_daily_pass_component__["a" /* DailyPassComponent */],
                __WEBPACK_IMPORTED_MODULE_4__local_pass_local_pass_component__["a" /* LocalPassComponent */],
                __WEBPACK_IMPORTED_MODULE_5__monthly_pass_monthly_pass_component__["a" /* MonthlyPassComponent */],
                __WEBPACK_IMPORTED_MODULE_6__return_pass_return_pass_component__["a" /* ReturnPassComponent */],
                __WEBPACK_IMPORTED_MODULE_7__single_pass_single_pass_component__["a" /* SinglePassComponent */],
                __WEBPACK_IMPORTED_MODULE_8__renewal_pass_renewal_pass_component__["a" /* RenewalPassComponent */]
            ],
            exports: []
        })
    ], PassesModule);
    return PassesModule;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/passes.routing-module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PassesRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daily_pass_daily_pass_component__ = __webpack_require__("./src/app/dashboard/passes/daily-pass/daily-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__local_pass_local_pass_component__ = __webpack_require__("./src/app/dashboard/passes/local-pass/local-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__monthly_pass_monthly_pass_component__ = __webpack_require__("./src/app/dashboard/passes/monthly-pass/monthly-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__return_pass_return_pass_component__ = __webpack_require__("./src/app/dashboard/passes/return-pass/return-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__single_pass_single_pass_component__ = __webpack_require__("./src/app/dashboard/passes/single-pass/single-pass.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__renewal_pass_renewal_pass_component__ = __webpack_require__("./src/app/dashboard/passes/renewal-pass/renewal-pass.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_4__monthly_pass_monthly_pass_component__["a" /* MonthlyPassComponent */] },
    { path: 'dailypass', component: __WEBPACK_IMPORTED_MODULE_2__daily_pass_daily_pass_component__["a" /* DailyPassComponent */] },
    { path: 'localpass', component: __WEBPACK_IMPORTED_MODULE_3__local_pass_local_pass_component__["a" /* LocalPassComponent */] },
    { path: 'monthlypass', component: __WEBPACK_IMPORTED_MODULE_4__monthly_pass_monthly_pass_component__["a" /* MonthlyPassComponent */] },
    { path: 'returnpass', component: __WEBPACK_IMPORTED_MODULE_5__return_pass_return_pass_component__["a" /* ReturnPassComponent */] },
    { path: 'singlepass', component: __WEBPACK_IMPORTED_MODULE_6__single_pass_single_pass_component__["a" /* SinglePassComponent */] },
    { path: 'renewalpass', component: __WEBPACK_IMPORTED_MODULE_7__renewal_pass_renewal_pass_component__["a" /* RenewalPassComponent */] },
];
var PassesRoutingModule = (function () {
    function PassesRoutingModule() {
    }
    PassesRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], PassesRoutingModule);
    return PassesRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/renewal-pass/renewal-pass.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"pass_types_main_section\">\r\n\t<div class=\"pass-option\">\r\n\t\t<a [routerLink]=\"['../dailypass']\">\r\n    \t\t<button>DAILY PASS</button>\r\n\t \t</a>\r\n\t\t<a [routerLink]=\"['../monthlypass']\">\r\n    \t\t<button>MONTHLY PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../localpass']\">\r\n    \t\t<button >LOCAL PASS</button>\r\n\t \t</a>\r\n\t \t<a [routerLink]=\"['../renewalpass']\">\r\n    \t\t<button class=\"active\">RENEWALS</button>\r\n\t \t</a>\r\n\t</div>\r\n\t<div class= \"flavour-range-div\">\r\n\t\t<div class=\"pass-flavour\">\r\n\t\t\t<div class=\"kilometer-range\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tBELOW 10 KM\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tBELOW 20 KM\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"month-range\">\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tONE MONTH\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tTWO MONTHS\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"pass-flavour__action_buttons\">\r\n\t\t\t\t\tSAMPLE\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\r\n\t\t</div>\r\n\t\t<div class=\"second-part\">\r\n\t\t\t<div class=\"date-trip-time\">\r\n\t\t\t\t<div class=\"current-date\">\r\n\t\t\t\t\t<P>11-08-2017  /  04:11 PM</P>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"date-validity\">\r\n\t\t\t\t\t<p>Date Validity:  11/08/2017 - 10/09/2017</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"trips-validity\">\r\n\t\t\t\t\t<p>Balance Validity:  40 ( Daily / Monthly )</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"total-text\">\r\n\t\t\t\t\t<span>Total Amount:</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"amount\">\r\n\t\t\t\t\t<span>&#8377;</span>\r\n\t\t\t\t\t<span>1500</span>\r\n\t\t\t\t\t<p>00</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"activate-pass pass__action_button\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\">ACTIVATE / DONE</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"back-button\">\r\n\t\t\t\t\t<button class= \"pass__default_btn\" [routerLink]=\"['../../../dashboard']\">BACK</button>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/passes/renewal-pass/renewal-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenewalPassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var RenewalPassComponent = (function () {
    function RenewalPassComponent() {
    }
    RenewalPassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/renewal-pass/renewal-pass.component.html"),
        })
    ], RenewalPassComponent);
    return RenewalPassComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/return-pass/return-pass.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/passes/return-pass/return-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReturnPassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ReturnPassComponent = (function () {
    function ReturnPassComponent() {
    }
    ReturnPassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/return-pass/return-pass.component.html"),
        })
    ], ReturnPassComponent);
    return ReturnPassComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/passes/single-pass/single-pass.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/passes/single-pass/single-pass.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SinglePassComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SinglePassComponent = (function () {
    function SinglePassComponent(commonserve, modalService, ref, dal_binding) {
        var _this = this;
        this.commonserve = commonserve;
        this.modalService = modalService;
        this.ref = ref;
        this.dal_binding = dal_binding;
        this.currentDateTime = new Date();
        this.dataInEpoc = this.currentDateTime.getTime() / 1000;
        this.passParams = {
            "readerType": "SCSPRO_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "sourceBranchId": "10601718",
            "destinationBranchId": "10510328",
            "periodicity": [{
                    "limitPeriodicity": "MONTHLY",
                    "limitMaxCount": 15
                },
                {
                    "limitPeriodicity": "DAILY",
                    "limitMaxCount": 2
                }],
            "expiryDate": 1530785820,
            "timeInMilliSeconds": this.dataInEpoc,
            "passType": "SINGLE",
            "maxTrips": 30,
            "isRenewal": false,
            "amount": 100
        };
        this.sdrInitializeVariable = {
            "readerType": "SCSPRO_READER",
            "readerIndex": 0,
            "serviceType": "TOLL",
            "branchId": "1060171"
        };
        this.dal_binding.stopDetection(this.sdrInitializeVariable, function (resultForRecharge) {
            console.log("sdr10 stop detection IN BUAROH", resultForRecharge);
        });
        this.dal_binding.activatePass(this.passParams, function (resultsdr) {
            console.log("pass result", resultsdr);
            _this.dal_binding.isCardRemoved(_this.sdrInitializeVariable, function (resp) {
                if (resp.cardRemoved == true) {
                    console.log("card removed from sdr10 in pass", resp);
                }
            });
        });
    }
    SinglePassComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/passes/single-pass/single-pass.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */],
            __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_3__services_dal_service__["a" /* DalService */]])
    ], SinglePassComponent);
    return SinglePassComponent;
}());



/***/ })

});
//# sourceMappingURL=passes.module.chunk.js.map