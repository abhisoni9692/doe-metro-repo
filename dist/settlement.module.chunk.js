webpackJsonp(["settlement.module"],{

/***/ "./src/app/dashboard/settlement/card-settlement/card-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n        <div class=\"button-style\">\r\n          <a [routerLink]=\"['../rate-card']\">\r\n            <button type=\"button\">Rate Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../transaction']\">\r\n            <button type=\"button\" class=\"\">Transaction</button>\r\n          </a>\r\n          <a [routerLink]=\"['../pass']\">\r\n            <button type=\"button\" class=\"\">Pass</button>\r\n          </a>\r\n          <a [routerLink]=\"['../recharge']\">\r\n            <button type=\"button\" class=\"\">Recharge</button>\r\n          </a>\r\n          <a>\r\n            <button type=\"button\" name=\"\" class=\"active\">Doe Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../summary']\">\r\n            <button type=\"button\" name=\"\">Summary</button>\r\n          </a>\r\n          <a [routerLink]=\"['../profile']\">\r\n            <button type=\"button\" name=\"\">Profile</button>\r\n          </a>\r\n        </div>\r\n        <div class=\"content-page\">\r\n          <div class=\"scrollable\">\r\n            <div class=\"settlement-card\">\r\n              <table class=\"first-table-settlement first-table-settlement-content\">\r\n                <thead>\r\n                  <tr class=\"\">\r\n                    <th class=\"\">SL. NO</th>\r\n                    <th>CARD NUMBER</th>\r\n                    <th>COST FOR CARD</th>\r\n                    <th>MERCHANT COMMISSION</th>\r\n                    <th>SALES AGENT COMMISSION</th>\r\n                    <th>KYC STATUS</th>\r\n                    <th>CARD ACTIVATION</th>\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>VERIFIED</td>\r\n                    <td><img src=\"../src/images/cross.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>VERIFIED</td>\r\n                    <td><img src=\"../src/images/cross.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/cross.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/cross.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>20.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>12243342222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>1224331242222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>1224331242222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>1224331242222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>1224331242222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                  <tr>\r\n                    <td>1</td>\r\n                    <td><span>&#8377;</span>1224331242222222</td>\r\n                    <td><span>&#8377;</span>2334.00</td>\r\n                    <td><span>&#8377;</span>30.00</td>\r\n                    <td><span>&#8377;</span>2000.00</td>\r\n                    <td>NOT VERIFIED</td>\r\n                    <td><img src=\"../src/images/right.png\" /></td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n            <!-- <div class=\"total-amount\">\r\n              <div class=\"card-summary-total\"><span class=\"total-amount-text\">Total</span></div>\r\n              <div class=\"card-sold-total\"><span class=\"total-card\">20</span> </div>\r\n              <div class=\"cost-for-card\"><span class=\"total-card-amount\">&#36; 3000</span></div>\r\n              <div class=\"merchant-agent-commission\"><span class=\"merchant-commission-total\">&#36; 360</span> </div>\r\n              <div class=\"sales-commission-total\"><span class=\"sales-commission-total\">&#36; 240</span> </div>\r\n              <div style=\"width:14%;\"></div>\r\n              <div class=\"card-activation\"><span class=\"card-activation-total\"> 12</span></div>\r\n            </div> -->\r\n            <!-- total summary sectn -->\r\n            <h3 class=\"amounts-div-header width-style\"> \r\n                TOTAL SUMMARY\r\n            </h3>\r\n            <table class=\"amounts-table last-width \">\r\n                <tr class=\"heading-backgrond-color-cards\">\r\n                    <td rowspan=\"2\">COUNT</td>\r\n                    <td rowspan=\"2\">TOTAL AMOUNT</td>\r\n                    <td colspan=\"3\">COMMISION</td>\r\n                    <td rowspan=\"2\">NET AMOUNT</td>\r\n                </tr>\r\n                <tr class=\"heading-backgrond-color-cards\">\r\n                    <td>MERCHANT</td>\r\n                    <td>SALES AGENT</td>\r\n                    <td>TDS</td>\r\n                </tr> \r\n                <tr class=\"content-backgrond-color-cards\">\r\n                    <td>20</td>\r\n                    <td>2000</td>\r\n                    <td>300</td>\r\n                    <td>200</td>\r\n                    <td>100</td>\r\n                    <td>2400</td>\r\n                </tr>\r\n            </table>\r\n            <!-- total summary div ends -->\r\n            <div class=\"details-division\">\r\n              <table class=\"details-table\">\r\n                <tr class=\"\">\r\n                  <th style=\"border-right:  1px solid #cccccc;\">MERCHANT</th>\r\n                  <th>SALES</th>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Company Name</span> <span class=\"details-input-text\">{{companyDetails.name}}</span></td>\r\n                  <td><span>Service Agent Name</span><span class=\"details-input-text\">{{operatorinfo.name}}</span></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Authorized Person Name</span><span class=\"details-input-text\">NA</span> </td>\r\n                  <td><span>BANK NAME</span><span class=\"details-input-text\">{{operatorinfo.bankAccount.bankName}}</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Phone Number</span><span class=\"details-input-text\">{{companyDetails.contact.details.landlineNumber}}</span> </td>\r\n                  <td><span>Account Number</span><span class=\"details-input-text\">{{operatorinfo.bankAccount.accountNumber}}</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>BANK NAME</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.bankName}}</span></td>\r\n                  <td><span>Name of Account</span><span class=\"details-input-text\">{{operatorinfo.bankAccount.type}}</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Account Number</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.accountNumber}}</span> </td>\r\n                  <td><span>IFSC Code</span><span class=\"details-input-text\">{{operatorinfo.bankAccount.IFSC}}</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Name of Account</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.type}}</span> </td>\r\n                  <td></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>IFSC Code</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.IFSC}}</span> </td>\r\n                  <td></td>\r\n                </tr>\r\n              </table>\r\n            </div>\r\n          </div>\r\n          <div class=\"settlement-buttons\">\r\n            <button type=\"button\" name=\"button\" class=\"dashboard-meter pull-left\" [routerLink]=\"['../../../dashboard']\">\r\n              <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n              <p>Dashboard</p>\r\n            </button>\r\n            <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\">\r\n              <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n              <p>Settlements</p>\r\n            </button>\r\n            <button type=\"button\" name=\"button\" class=\"send-report pull-left\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n              <p>Send Report</p>\r\n            </button>\r\n            <div class=\"profile-calender\" style=\"height: 100%; position: relative; width: 23%;\" >\r\n              <div style=\"display:flex; flex-direction: row; justify-content:space-between; height: 100%;width: 100%;\">\r\n                <div class='input-daterange ' style=\"width: 54%;\">\r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-to\" placeholder=\"Start date\" name=\"dp\"[(ngModel)]=\"startdate_model\" ngbDatepicker #d=\"ngbDatepicker\" (click)=\"d.toggle(); e.close();\" type=\"text\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n                <div class='input-daterange'style=\"width: 54%;\"> \r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-from\" placeholder=\"End date\" name=\"dq\"[(ngModel)]=\"enddate_model\" ngbDatepicker #e=\"ngbDatepicker\" (click)=\"d.close(); e.toggle();\" type=\"text\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"page-upto\"><span> 1-19 of 100</span><img src=\"../src/images/leftarrow.png\" alt=\"\" class=\"pull-right\" /> <img src=\"../src/images/rightarrow.png\" alt=\"\" class=\"pull-right\" /> </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      "

/***/ }),

/***/ "./src/app/dashboard/settlement/card-settlement/card-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardSettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CardSettlementComponent = (function () {
    function CardSettlementComponent(commonserve, activateroute, route, base64, dal) {
        var _this = this;
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.route = route;
        this.base64 = base64;
        this.dal = dal;
        this.tollInfo = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].tollInfo;
        this.companyDetails = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].companyInfo;
        this.operatorinfo = __WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].operatorDetails;
        this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].sdrInitializeVariable, function (resultsdr) {
            console.log("sdr10 stopDetection in beginning", resultsdr);
            _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].buarohinitializeVariable, function (resultbuaroh) {
                _this.dal.stopDetection(__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].buarohinitializeVariable2, function (resultbuaroh2) {
                    console.log(" buaroh2 stopDetection in beginning", resultbuaroh2);
                });
                console.log(" buaroh stopDetection in beginning", resultbuaroh);
            });
        });
    }
    CardSettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/card-settlement/card-settlement.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3__services_base64_service__["a" /* Base64Service */], __WEBPACK_IMPORTED_MODULE_4__services_dal_service__["a" /* DalService */]])
    ], CardSettlementComponent);
    return CardSettlementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/pass-settlement/pass-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n  <div class=\"button-style\">\r\n    <a [routerLink]=\"['../rate-card']\">\r\n      <button type=\"button\">Rate Card</button>\r\n    </a>\r\n    <a [routerLink]=\"['../transaction']\">\r\n      <button type=\"button\" class=\"\">Transaction</button>\r\n    </a>\r\n    <a>\r\n      <button type=\"button\" class=\"active\">Pass</button>\r\n    </a>\r\n    <a [routerLink]=\"['../recharge']\">\r\n      <button type=\"button\" class=\"\">Recharge</button>\r\n    </a>\r\n    <a [routerLink]=\"['../cardsettlement']\">\r\n      <button type=\"button\" name=\"\" class=\"\">Doe Card</button>\r\n    </a>\r\n    <a [routerLink]=\"['../summary']\">\r\n      <button type=\"button\" name=\"\">Summary</button>\r\n    </a>\r\n    <a [routerLink]=\"['../profile']\">\r\n      <button type=\"button\" name=\"\">Profile</button>\r\n    </a>\r\n  </div>\r\n  <div class=\"content-page\">\r\n    <div *ngIf=\"noTransactionMesage\" class=\"amounts-div-header no-margin-to\">\r\n      <span>{{noTransactionMesage}} </span>\r\n    </div>\r\n    <div *ngIf=\"!noTransactionMesage\" class=\"scrollable\">\r\n      <div class=\"settlement-card\">\r\n        <table class=\"first-table-settlement first-table-settlement-content\">\r\n          <thead>\r\n            <tr class=\"\">\r\n              <th class=\"\">SL. NO</th>\r\n              <th>CARD NUMBER</th>\r\n              <th>PASS TYPE</th>\r\n              <th>NO. OF TRIPS / MONTHLY</th>\r\n              <th>ACTIVITY TILL</th>\r\n              <th>DUMMY</th>\r\n              <th>DUMMY</th>\r\n              <th>AMOUNT</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr *ngFor=\"let item of recharge_logs.activities; let i = index\">\r\n                <td *ngIf=\"!item.id\">NA </td> <td *ngIf=\"item.id\">{{i+1}}</td>\r\n                <td *ngIf=\"!item.cardNumber\">NA</td> <td *ngIf=\"item.cardNumber\">{{item.cardNumber}}</td>\r\n                <td *ngIf=\"!item.action\">NA</td> <td *ngIf=\"item.action\">{{item.action}}</td>\r\n                <td *ngIf=\"!item.remainingTrips\">NA</td> <td *ngIf=\"item.remainingTrips\">{{item.remainingTrips}}</td>\r\n                <td *ngIf=\"!item.activityTime\">NA</td> <td *ngIf=\"item.activityTime\">{{getDate(item.activityTime)}}</td>\r\n                <td>--</td>\r\n                <td>--</td>\r\n                <td *ngIf=\"!item.amount.value\">&#8377; NA</td> <td *ngIf=\"item.amount.value\">&#8377; {{item.amount.value}}</td>\r\n            </tr> \r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <div class=\"total-amount\">\r\n        <div class=\"card-summary-total\"><span class=\"total-amount-text\">Total</span></div>\r\n        <div class=\"card-sold-total\"><span class=\"total-card\">--</span> </div>\r\n        <div class=\"cost-for-card\"><span class=\"total-card-amount\">&#36; --</span></div>\r\n        <div class=\"merchant-agent-commission\"><span class=\"merchant-commission-total\">&#36; --</span> </div>\r\n        <div class=\"sales-commission-total\"><span class=\"sales-commission-total\">&#36; --</span> </div>\r\n        <div style=\"width:14%;\"></div>\r\n        <div class=\"card-activation\"><span *ngIf= \"totalRecharge\" class=\"card-activation-total\"> {{totalRecharge}}</span> <span *ngIf= \"!totalRecharge\" class=\"card-activation-total\"> -- </span></div>\r\n      </div>\r\n    </div>\r\n    <div class=\"settlement-buttons\">\r\n      <button type=\"button\" name=\"button\" class=\"dashboard-meter pull-left\" [routerLink]=\"['../../../dashboard']\">\r\n        <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n        <p>Dashboard</p>\r\n      </button>\r\n      <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\">\r\n        <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n        <p>Settlements</p>\r\n      </button>\r\n      <button type=\"button\" name=\"button\" class=\"send-report pull-left\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n        <p>Send Report</p>\r\n      </button>\r\n      <div class=\"profile-calender\" style=\"height: 100%; position: relative; width: 23%;\" >\r\n        <div style=\"display:flex; flex-direction: row; justify-content:space-between; height: 100%;width: 100%;\">\r\n          <div class='input-daterange ' style=\"width: 54%;\">\r\n            <!-- ngb date -->\r\n            <input class=\"calender-to\" placeholder=\"Start date\" name=\"dp\"[(ngModel)]=\"startdate_model\" ngbDatepicker #d=\"ngbDatepicker\" (click)=\"d.toggle(); e.close();\" type=\"text\" (dateSelect)=\"onDateStartSelect($event)\" [minDate]=\"minDate \"\r\n            [maxDate]=\"maxDate\" readonly>\r\n              <!-- ngb date -->\r\n            </div>\r\n            <div class='input-daterange'style=\"width: 54%;\"> \r\n              <!-- ngb date -->\r\n              <input class=\"calender-from\" placeholder=\"End date\" name=\"dq\"[(ngModel)]=\"enddate_model\" ngbDatepicker #e=\"ngbDatepicker\" (click)=\"d.close(); e.toggle()\" type=\"text\" (dateSelect)=\"onDateEndSelect($event);\" [minDate]=\"minDate\"\r\n            [maxDate]=\"maxDate\" readonly>\r\n            <!-- ngb date -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"page-upto\"><span *ngIf=\"!noTransactionMesage\" > {{pagination.pageno}} of {{totalItems}}</span> <span *ngIf=\"noTransactionMesage\" >0</span> <a (click)=\"nextItems('PREVIOUS')\"><img src=\"../src/images/leftarrow.png\" alt=\"\" class=\"pull-right\" /></a> <a (click)=\"nextItems('NEXT')\"><img src=\"../src/images/rightarrow.png\" alt=\"\" class=\"pull-right\" /> </a></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n      "

/***/ }),

/***/ "./src/app/dashboard/settlement/pass-settlement/pass-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PassSettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PassSettlementComponent = (function () {
    function PassSettlementComponent(commonserve, activateroute, route, base64) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.route = route;
        this.base64 = base64;
        this.tollInfo = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].tollInfo;
        this.companyDetails = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].companyInfo;
        this.operatorinfo = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].operatorDetails;
        this.transaction_logs = {
            activities: []
        };
        this.recharge_logs = {
            activities: []
        };
        this.pagination = {
            "pageno": 1,
            "pagesize": 10
        };
        this.flag = 0;
        this.lastLogs = 0;
        this.getMaxMinDate();
        this.getCurrentDate();
        this.getTodayRechargeLogs();
    }
    PassSettlementComponent.prototype.onDateStartSelect = function (startchange) {
        console.log("startchange chnge......jjjj", this.startchange);
        console.log("start date", startchange);
        if (JSON.stringify(this.startchange) == JSON.stringify(startchange)) {
        }
        else {
            this.pagination.pageno = 1;
        }
        this.startchange = {
            "year": String(startchange.year).padStart(2, '0'),
            "month": String(startchange.month).padStart(2, '0'),
            "day": String(startchange.day).padStart(2, '0')
        };
    };
    PassSettlementComponent.prototype.getMaxMinDate = function () {
        var date = new Date();
        this.maxDate = {
            "year": date.getFullYear(),
            "month": date.getMonth() + 1,
            "day": date.getDate()
        };
        var priorDateInMilisecond = date.setMonth(date.getMonth() - 5);
        var priorDate = new Date(priorDateInMilisecond);
        this.minDate = {
            "year": priorDate.getFullYear(),
            "month": priorDate.getMonth(),
            "day": priorDate.getDate()
        };
    };
    PassSettlementComponent.prototype.onDateEndSelect = function (endchange) {
        var _this = this;
        if (!this.startchange) {
            alert("Please enter Start Date");
        }
        else {
            this.lastSelectDate = endchange;
            this.lastLogs = 1;
            var today = new Date();
            var ISOString = today.toISOString();
            var todayDate = this.getDate(ISOString);
            var endchangeTemp = String(endchange.year).padStart(2, '0') + "-" + String(endchange.month).padStart(2, '0') + "-" + String(endchange.day).padStart(2, '0');
            if (endchangeTemp == todayDate) {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day + 1).padStart(2, '0')
                };
            }
            else {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day).padStart(2, '0')
                };
            }
            var tempEndChange = this.endchange;
            tempEndChange = {
                "year": String(endchange.year).padStart(2, '0'),
                "month": String(endchange.month).padStart(2, '0'),
                "day": String(endchange.day + 1).padStart(2, '0')
            };
            console.log("this.endchange", tempEndChange);
            if (JSON.stringify(this.endchange) !== JSON.stringify(tempEndChange) && this.flag == 1) {
                this.pagination.pageno = 1;
            }
            var queryparam = "?time_slice=lastthreemonths&start_time=" + this.startchange.year + "-" + this.startchange.month + "-" + this.startchange.day + "&end_time=" + this.endchange.year + "-" + this.endchange.month + "-" + this.endchange.day + "&page_size=10&actions=LOCAL_PASS_PURCHASE,MONTHLY_PASS_PURCHASE,LOCAL_PASS_RENEW,MONTHLY_PASS_RENEW" + "&page=" + this.pagination.pageno;
            this.commonserve.transactionLogsApi(queryparam)
                .subscribe(function (response) {
                console.log("recharge log working with date chnge......", response);
                if (response['totalItems'] == 0) {
                    _this.noTransactionMesage = "No Transaction";
                }
                else {
                    _this.noTransactionMesage = "";
                    _this.recharge_logs = response;
                    console.log("recharge log working......", _this.recharge_logs.activities);
                    _this.totalItems = _this.recharge_logs['totalPages'];
                }
            });
        }
    };
    //convert date of Tz format and show in UI
    PassSettlementComponent.prototype.getDate = function (item) {
        var temp = String(item).split('T');
        var date = temp[0];
        return date;
    };
    //ends
    //restrict customer to select date with 6 months
    PassSettlementComponent.prototype.getCurrentDate = function () {
        var today = new Date();
        this.toISOString = today.toISOString();
        var dd = String(today.getDate()).padStart(2, '0');
        var nextDay = String(today.getDate() - 1).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        this.currentDate = {
            "dd": dd,
            "nextDay": nextDay,
            "mm": mm,
            "yyyy": yyyy
        };
        today.setMonth(today.getMonth() - 3);
        var localdate = today.toLocaleDateString();
        var arrayStartDate = localdate.split('/');
        var startday = arrayStartDate[0].padStart(2, '0');
        var startmonth = arrayStartDate[1].padStart(2, '0');
        var startyear = arrayStartDate[2];
        this.threeMonthPrior = {
            "mm": startday,
            "dd": startmonth,
            "yyyy": startyear
        };
        console.log("three month before", this.threeMonthPrior);
    };
    //get recharge transaction logs
    PassSettlementComponent.prototype.getTodayRechargeLogs = function () {
        var _this = this;
        var queryparam = "?time_slice=lastthreemonths&start_time=" + this.currentDate.yyyy + "-" + this.currentDate.mm + "-" + this.currentDate.dd + "&end_time=" + this.toISOString + "&page_size=" + this.pagination.pagesize + "&actions=LOCAL_PASS_PURCHASE,MONTHLY_PASS_PURCHASE,LOCAL_PASS_RENEW,MONTHLY_PASS_RENEW" + "&page=" + this.pagination.pageno;
        this.commonserve.transactionLogsApi(queryparam)
            .subscribe(function (response) {
            if (response['totalPages'] == 0) {
                _this.noTransactionMesage = "No Transaction";
            }
            else {
                _this.noTransactionMesage = "";
                _this.recharge_logs = response;
                console.log("recharge log working......", _this.recharge_logs.activities);
                _this.totalItems = _this.recharge_logs['totalPages'];
            }
        });
    };
    //ends
    PassSettlementComponent.prototype.nextItems = function (type) {
        if (type == 'NEXT') {
            if (this.pagination.pageno < this.totalItems) {
                this.pagination.pageno = this.pagination.pageno + 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayRechargeLogs();
            }
        }
        else {
            if (this.pagination.pageno > 1) {
                this.pagination.pageno = this.pagination.pageno - 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayRechargeLogs();
            }
        }
    };
    PassSettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/pass-settlement/pass-settlement.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3__services_base64_service__["a" /* Base64Service */]])
    ], PassSettlementComponent);
    return PassSettlementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n        <div class=\"button-style\">\r\n          <a [routerLink]=\"['../rate-card']\">\r\n            <button type=\"button\">Rate Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../transaction']\">\r\n            <button type=\"button\" class=\"\">Transaction</button>\r\n          </a>\r\n          <a [routerLink]=\"['../pass']\">\r\n            <button type=\"button\" class=\"\">Pass</button>\r\n          </a>\r\n          <a [routerLink]=\"['../recharge']\">\r\n            <button type=\"button\" class=\"\">Recharge</button>\r\n          </a>\r\n          <a [routerLink]=\"['../cardsettlement']\">\r\n            <button type=\"button\" name=\"\">Doe Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../summary']\">\r\n            <button type=\"button\" name=\"\">Summary</button>\r\n          </a>\r\n          <a>\r\n            <button type=\"button\"  class=\"active\" name=\"\">Profile</button>\r\n          </a>\r\n        </div>\r\n        <div class=\"full-content\">\r\n            <div class=\"left-content\">\r\n    \r\n                <div class=\"profile-main\" *ngIf=\"operatorDetails\">\r\n                    <div class=\"basic-info-text\">\r\n                        <span>BASIC INFO</span>\r\n                        <img src=\"../src/images/edit-img.png\" class=\"edit-icon\" />\r\n                    </div>\r\n                    <div class=\"upper-section\">\r\n                      <div class=\"common-row\">\r\n                          <div class=\"common-section\">\r\n                            <div class=\"total-credit-img\">\r\n                                <img class=\"total-credit-img-circle\" src=\"../src/images/parking@3x.png\" />\r\n                            </div>\r\n                            <div class=\"total-credit-amount\">\r\n                                <p class=\"total-credit\">\r\n                                    TOTAL CREDITS\r\n                                </p>\r\n                                <p class=\"credit-total-amount\">\r\n                                  <sup>&#8377;</sup>1500<sup>00 </sup>\r\n                                </p>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"common-section\">\r\n                            <div class=\"common-inner-content\">\r\n                                <p class=\"same-upper-p\">\r\n                                  Name\r\n                                </p>\r\n                                <p class=\"same-lower-p\">\r\n                                  {{operatorDetails.name}}\r\n                                </p>\r\n                            </div>\r\n                          </div>\r\n                      </div>\r\n                      <div class=\"common-row\">\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                USER ID\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.loginIdentifiers.username.identifier}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                EMAIL ID\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.businessProfile.email}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n    \r\n                      <div class=\"common-row\">\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                LOCATION\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                NA\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                AADHAR NUMDER\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.aadhaar}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n    \r\n    \r\n                    </div>\r\n                    <div class=\"bank-detail-text\">\r\n                        <span>BANK DETAILS</span>\r\n    \r\n                    </div>\r\n                    <div class=\"upper-section\">\r\n                      <div class=\"common-row\">\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                ACCOUNT NUMBER\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.bankAccount.accountNumber}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                  BANK NAME\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.bankAccount.bankName}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"common-row\">\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                IFSC CODE\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.bankAccount.IFSC}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                BRANCH\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                NA\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"common-row\">\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                MOBILE NUMBER 1\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                {{operatorDetails.businessProfile.mobileNumber}}\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"common-section\">\r\n                          <div class=\"common-inner-content\">\r\n                              <p class=\"same-upper-p\">\r\n                                MOBILE NUMBER 2\r\n                              </p>\r\n                              <p class=\"same-lower-p\">\r\n                                NA\r\n                              </p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n    \r\n                </div>\r\n    \r\n                <div class=\"overview1\">\r\n                    <button type=\"button\" [routerLink]=\"['../../../dashboard']\" name=\"button\" class=\"overview__dashboard-meter\">\r\n                        <img src=\"../src/images/dashboard-icon.png\" alt=\"\" />\r\n                        <p>Dashboard</p>\r\n                    </button>\r\n                    <button type=\"button\" name=\"button\" class=\"overview__settlements\">\r\n                        <img src=\"../src/images/settlements-icon.png\" alt=\"\" />\r\n                        <p>Settlements</p>\r\n                    </button>\r\n                    <button type=\"button\" name=\"button\" class=\"logout-button\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\" (click)= \"logout();\">\r\n                        <img src=\"../src/images/logout.png\" alt=\"\" />\r\n                        <p>Logout</p>\r\n                    </button>\r\n                    <button type=\"button\" name=\"button\" class=\"update-account\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n                        <p>Update Account Settings</p>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n            <div class=\"second-division\">\r\n                <div class=\"your-last-text\"> YOUR LAST 10 CREDITS </div>\r\n                    <ul>\r\n                        <li class=\"li-style\" ng-repeat=\"transactions in recentTranasction.data.data.activities\">\r\n                            <div>\r\n                                <div>\r\n                                        <span class=\"p-style \">Commission for</span>\r\n                                </div>\r\n                                <div>\r\n                                    <span class=\"p-account-number \"></span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"amount-li\">\r\n                                <span class=\"p-amount \"><sup>&#8377;</sup></span>\r\n                            </div>\r\n                        </li>\r\n                        \r\n                    </ul>\r\n    \r\n            </div>\r\n        </div>\r\n    </div>\r\n    "

/***/ }),

/***/ "./src/app/dashboard/settlement/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfileComponent = (function () {
    function ProfileComponent(commonserve, activateroute, router, base64) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.router = router;
        this.base64 = base64;
        this.getOperatorDetails();
    }
    ProfileComponent.prototype.getOperatorDetails = function () {
        var _this = this;
        this.commonserve.operatorInfo(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].businessLevelId.sub).subscribe(function (response) {
            console.log("operator info", response);
            _this.operatorDetails = response;
        }, function (err) {
            console.log("error in login ", err);
        });
    };
    ProfileComponent.prototype.logout = function () {
        var _this = this;
        this.commonserve.logoutApi().subscribe(function (response) {
            console.log("logout operator", response);
            _this.commonserve.deleteOperationInfo();
            _this.router.navigate(['/home']);
        }, function (err) {
            console.log("error in logout ", err);
        });
    };
    ;
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/profile/profile.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4__services_base64_service__["a" /* Base64Service */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/rate-card-settlement/rate-card-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n        <div class=\"button-style\">\r\n          <a>\r\n            <button type=\"button\" class=\"active\">Rate Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../transaction']\">\r\n            <button type=\"button\" class=\"\">Transaction</button>\r\n          </a>\r\n          <a [routerLink]=\"['../pass']\">\r\n            <button type=\"button\" class=\"\">Pass</button>\r\n          </a>\r\n          <a [routerLink]=\"['../recharge']\">\r\n            <button type=\"button\" class=\"\">Recharge</button>\r\n          </a>\r\n          <a [routerLink]=\"['../cardsettlement']\">\r\n            <button type=\"button\" name=\"\">Doe Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../summary']\">\r\n            <button type=\"button\" name=\"\">Summary</button>\r\n          </a>\r\n          <a [routerLink]=\"['../profile']\">\r\n            <button type=\"button\" name=\"\">Profile</button>\r\n          </a>\r\n        </div>\r\n        <div class=\"content-page\">\r\n            <div class=\"scrollable rate-scroll\">\r\n                <table class=\"first-transaction-table\">\r\n                    <tr>\r\n                        <th>MERCHANT NAME</th>\r\n                        <th>TOLL GATE DETAILS</th>\r\n                        <th>FEE EFFECTIVE DATE</th>\r\n                    </tr>\r\n                    <tr *ngIf = \"feesetup\">\r\n                        <td>\r\n                            <p>Name of Concessionaire / OMT Contractor : {{companyDetails.name}}.<br> Name / Contact Details of Incharge : {{companyDetails.contact.details.landlineNumber}}</p>\r\n                        </td>\r\n                        <td>\r\n                            <p>{{tollInfo.items[0].name}}<br>{{tollInfo.items[0].location.area}}<br>Km{{tollInfo.items[0].location.kiloMeter}}.{{tollInfo.items[0].location.highwayType}} IN {{tollInfo.items[0].location.state}}<br>Stretch : {{tollInfo.items[0].location.stretch}}<br>Tollable Length : {{tollInfo.items[0].location.tollableLength}}\r\n                            </p>\r\n                        </td>\r\n                        <td> \r\n                            <p>Fee effective date : {{effectiveFrom}} <br> Due date of Toll revision : {{effectiveTill}} </p>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"second-scroll scroll-style\" >\r\n                    <table class=\"second-transaction-table left-adjust\" *ngIf = \"feesetup\">\r\n    \r\n                        <tr>\r\n                            <th>Type of Vehicle</th>\r\n                            <th *ngFor=\"let jrny of feesetup.journeyTypes\">{{jrny.label}}</th>\r\n                        </tr>\r\n                        <tr *ngFor=\"let vehicle of feesetup.vehicleTypes\">\r\n                            <td>{{vehicle.label}}</td>\r\n                            <td *ngFor=\"let jrny of feesetup.journeyTypes; let i = index\">&#8377;\r\n                                <span *ngIf=\"feesetup.structure[vehicle.code][jrny.code]\">{{feesetup.structure[vehicle.code][jrny.code].value}}</span>\r\n                                <span *ngIf=\"!feesetup.structure[vehicle.code][jrny.code]\">_.__ __</span>\r\n                            </td>\r\n                        </tr>\r\n    \r\n                    </table>\r\n                </div>\r\n    \r\n               \r\n            </div>\r\n            <div class=\"rate-footer\">\r\n                <button type=\"button\" [routerLink]=\"['../../../dashboard']\" name=\"button\" class=\"dashboard-meter pull-left\">\r\n                    <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n                    <p>Dashboard</p>\r\n                </button>\r\n                <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\">\r\n                    <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n                    <p>Settlements</p>\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    "

/***/ }),

/***/ "./src/app/dashboard/settlement/rate-card-settlement/rate-card-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RateCardSettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dal_service__ = __webpack_require__("./src/app/services/dal.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RateCardSettlementComponent = (function () {
    function RateCardSettlementComponent(commonserve, activateroute, route, base64, dal) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.route = route;
        this.base64 = base64;
        this.dal = dal;
        this.categoryType = "NH";
        this.activatedroute = this.activateroute;
        this.branchId = this.activatedroute.parent.params._value.id;
        this.error = {};
        this.error.showmeassge = false;
        this.error.message = "Kindly atleast one from each Vehicle type and service tupe";
        this.feesetup = {};
        this.effectiveFrom = "";
        this.effectiveTill = "";
        this.companyid = this.activatedroute.parent.parent.params._value.id;
        this.tollInfo = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].tollInfo;
        this.companyDetails = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].companyInfo;
        this.getOperatorDetails();
        this.gettoll();
    }
    RateCardSettlementComponent.prototype.gettoll = function () {
        var _this = this;
        var queryparam = "?business_level_id=" + Object.keys(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].businessLevelId['businessProfile'])[0];
        this.commonserve.apigetDomaindata(queryparam)
            .subscribe(function (response) {
            console.log("toll info=======================>>");
            _this.domaindataresponse = response;
            _this.feesetup.tollInfo = _this.domaindataresponse && _this.domaindataresponse.items && _this.domaindataresponse.items[0] ? _this.domaindataresponse.items[0] : {};
            console.log(response);
            for (var i = 0; i < _this.feesetup.tollInfo.feeRevisions.length; i++) {
                var effectiveDate = new Date(_this.feesetup.tollInfo.feeRevisions[i].effectiveTill).getTime();
                var todayDate = new Date().getTime();
                if (todayDate < effectiveDate) {
                    _this.feesetup.feeStructure = _this.feesetup.tollInfo.feeRevisions[i];
                    console.log("fee structure ===========>>");
                    console.log(_this.feesetup.feeStructure);
                    _this.updateFeeStructure();
                }
            }
        });
    };
    RateCardSettlementComponent.prototype.getOperatorDetails = function () {
        // var businessLevelId;
        var auth = this.commonserve.getStoreData();
        console.log("token is", JSON.stringify(auth));
        var str = auth.accesstoken.split(".");
        var decodestr = str[1];
        var userinfo = this.base64.decode(decodestr);
        console.log("userinfo.....", userinfo);
        var s = userinfo.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        s = s.replace(/[\u0000-\u0019]+/g, "");
        var obj = JSON.parse(s);
        var useriD = obj.sub;
        console.log("user id", useriD);
        this.commonserve.operatorInfo(useriD).subscribe(function (response) {
            console.log("operator info", response);
        }, function (err) {
            console.log("error in login ", err);
        });
    };
    ;
    RateCardSettlementComponent.prototype.updateFeeStructure = function () {
        var _this = this;
        this.feesetup.vehicleTypes = [];
        this.feesetup.journeyTypes = [];
        this.feesetup.structure = {};
        if (this.feesetup.feeStructure.baseFees) {
            this.feesetup.feeStructure.baseFees.forEach(function (item, index) {
                if (_this.feesetup.structure[item.categoryCode]) {
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                else {
                    _this.feesetup.structure[item.categoryCode] = {};
                    _this.feesetup.structure[item.categoryCode][item.activityCode] = {
                        "id": item.id,
                        "data": item,
                        "value": item.amount.value
                    };
                }
                var jindex = _this.feesetup.journeyTypes.findIndex(function (x) { return x.code == item.activityCode; });
                if (!(jindex > -1)) {
                    _this.feesetup.journeyTypes.push({
                        "code": item.activityCode,
                        "label": item.activityCodeInfo.label.replace("_", ""),
                        "info": item.activityCodeInfo
                    });
                }
                var vindex = _this.feesetup.vehicleTypes.findIndex(function (y) { return y.code == item.categoryCode; });
                if (!(vindex > -1)) {
                    var label = item.categoryCodeInfo.label.split('_');
                    _this.feesetup.vehicleTypes.push({
                        "code": item.categoryCode,
                        "label": label[1],
                        "info": item.categoryCodeInfo
                    });
                    console.log("search ing", _this.feesetup.vehicleTypes);
                }
            });
        }
        console.log("feesetup is", this.feesetup);
        //convert ISO Date format to date 
        this.effectiveFrom = this.feesetup.feeStructure.effectiveFrom.split("T")[0];
        this.effectiveTill = this.feesetup.feeStructure.effectiveTill.split("T")[0];
    };
    RateCardSettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/rate-card-settlement/rate-card-settlement.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_5__services_base64_service__["a" /* Base64Service */], __WEBPACK_IMPORTED_MODULE_4__services_dal_service__["a" /* DalService */]])
    ], RateCardSettlementComponent);
    return RateCardSettlementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/recharge-settlement/recharge-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n    <div class=\"button-style\">\r\n      <a [routerLink]=\"['../rate-card']\">\r\n        <button type=\"button\">Rate Card</button>\r\n      </a>\r\n      <a [routerLink]=\"['../transaction']\">\r\n        <button type=\"button\" class=\"\">Transaction</button>\r\n      </a>\r\n      <a [routerLink]=\"['../pass']\">\r\n        <button type=\"button\" class=\"\">Pass</button>\r\n      </a>\r\n      <a [routerLink]=\"['../recharge']\">\r\n        <button type=\"button\" class=\"active\">Recharge</button>\r\n      </a>\r\n      <a [routerLink]=\"['../cardsettlement']\">\r\n        <button type=\"button\" name=\"\">Doe Card</button>\r\n      </a>\r\n      <a [routerLink]=\"['../summary']\">\r\n        <button type=\"button\" name=\"\">Summary</button>\r\n      </a>\r\n      <a [routerLink]=\"['../profile']\">\r\n        <button type=\"button\" name=\"\">Profile</button>\r\n      </a>\r\n    </div>\r\n        <div class=\"content-page\">\r\n            <div class=\"button-style tabSuccessUnsuccess\" >\r\n              <a (click)= \"getTodayRechargeLogs()\">\r\n                <button type=\"button\">SUCCESSFULL</button>\r\n              </a>\r\n              <a (click)= \"showUnSuccessfulRecharge()\">\r\n                <button type=\"button\" class=\"\">DEFERRED/UNSUCCESSFULL</button>\r\n              </a>\r\n            </div>\r\n            <div *ngIf=\"noTransactionMesage\" class=\"amounts-div-header no-margin-to\">\r\n              <span>{{noTransactionMesage}} </span>\r\n            </div>\r\n            <div class=\"scrollable rechargeScrollabel\" *ngIf=\"!noTransactionMesage\">\r\n                <div class=\"fourth-scroll scroll-style \">\r\n                    <table class=\"recharge-transaction-table left-adjust\" >\r\n                        <tr>\r\n                            <th>S NO.</th>\r\n                            <th>DATE</th>\r\n                            <th>CARD NUMBER</th>\r\n                            <th>AMOUNT</th>\r\n                        </tr>\r\n                        <tr  *ngFor=\"let item of recharge_logs.activities; let i = index\">\r\n                            <td *ngIf=\"!item.id\">NA </td> <td *ngIf=\"item.id\">{{i+1}}</td>\r\n                            <td *ngIf=\"!item.activityTime\">NA</td> <td *ngIf=\"item.activityTime\">{{getDate(item.activityTime)}}</td>\r\n                            <td *ngIf=\"!item.cardNumber\">NA</td> <td *ngIf=\"item.cardNumber\">{{item.cardNumber}}</td>\r\n                            <td *ngIf=\"!item.amount.value\">&#8377; NA</td> <td *ngIf=\"item.amount.value\">&#8377; {{item.amount.value}}</td>\r\n                        </tr>\r\n                    </table>\r\n                </div>\r\n                <!-- <div class=\"total-recharge-div backgroung-green\" *ngIf=\"!noTransactionMesage\">\r\n                    <div class=\"total-text\"><span class=\"total-amount-text\">Total </span> </div>\r\n                    <div class=\"total-card-recharge\"><span class=\"total-card\"></span></div>\r\n                    <div class=\"total-recharge-sum\"><span *ngIf= \"!totalRecharge\" class=\" recharge-total-amount\">&#8377; -- </span><span *ngIf= \"totalRecharge\" class=\" recharge-total-amount\">&#8377; </span>{{totalRecharge}}</div>\r\n                </div> -->\r\n                <!--amount division-->\r\n                <!-- total summary sectn -->\r\n                <h3 class=\"amounts-div-header width-style\"> \r\n                    TOTAL SUMMARY\r\n                </h3>\r\n                <table class=\"amounts-table last-width \">\r\n                    <tr class=\"heading-backgrond-color-cards\">\r\n                        <td>COUNT</td>\r\n                        <td>TOTAL AMOUNT</td>\r\n                        <td>INSENTIVE</td>\r\n                        <td>GST</td>\r\n                        <td>TDS</td>\r\n                        <td>TDS</td>\r\n                        <td>NET AMOUNT</td>\r\n                    </tr> \r\n                    <tr class=\"content-backgrond-color-cards\">\r\n                        <td>20</td>\r\n                        <td>2000</td>\r\n                        <td>300</td>\r\n                        <td>200</td>\r\n                        <td>100</td>\r\n                        <td>2400</td>\r\n                        <td>100</td>\r\n                    </tr>   \r\n                </table>\r\n              <!-- total summary div ends -->\r\n\r\n            <!-- merchant section -->\r\n            <h3 class=\"amounts-div-header width-style\"> \r\n                    MERCHANT\r\n            </h3>\r\n            <div class=\"details-division\">\r\n              <table class=\"details-table\">\r\n                <tr>\r\n                  <td><span>Company Name</span> <span class=\"details-input-text\">{{companyDetails.name}}</span></td>\r\n                  <td><span>Authorized Person Name</span><span *ngIf= !companyDetails.merchantname class=\"details-input-text\">NA</span><span *ngIf=\"companyDetails.merchantname\" class=\"details-input-text\">{{companyDetails.merchantname}}</span></td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>Name of Account</span><span *ngIf=\"!companyDetails.settlement.details.registeredName\" class=\"details-input-text\">NA</span>\r\n                  <span *ngIf=\"companyDetails.settlement.details.registeredName\" class=\"details-input-text\">{{companyDetails.settlement.details.registeredName}}</span>\r\n                   </td>\r\n                  <td><span>Phone Number</span><span class=\"details-input-text\" *ngIf=\"companyDetails.contact.details.landlineNumber\">{{companyDetails.contact.details.landlineNumber}}</span> \r\n                  <span class=\"details-input-text\" *ngIf=\"!companyDetails.contact.details.landlineNumber\">NA</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>BANK Name</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.bankName}}</span> </td>\r\n                  <td><span>Account Number</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.accountNumber}}</span> </td>\r\n                </tr>\r\n                <tr>\r\n                  <td><span>IFSC Code</span><span class=\"details-input-text\">{{companyDetails.settlement.details.bankAccount.IFSC}}</span></td>\r\n                  <td><span></span><span class=\"details-input-text\"></span> </td>\r\n                </tr>\r\n              </table>\r\n            </div>\r\n            <!-- merchant section ends -->  \r\n            </div>\r\n\r\n            <!--footer-->\r\n            <div class=\"settlement-buttons\">\r\n              <button type=\"button\" [routerLink]=\"['../../../dashboard']\" name=\"button\" class=\"dashboard-meter pull-left\">\r\n                <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n                <p>Dashboard</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\" (click)=\"showtime()\">\r\n                <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n                <p>Settlements</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"send-report pull-left\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n                <p>Send Report</p>\r\n              </button>\r\n              <div class=\"profile-calender\" style=\"height: 100%; position: relative; width: 23%;\" >\r\n              <div style=\"display:flex; flex-direction: row; justify-content:space-between; height: 100%;width: 100%;\">\r\n                <div class='input-daterange ' style=\"width: 54%;\">\r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-to\" placeholder=\"Start date\" name=\"dp\"[(ngModel)]=\"startdate_model\" ngbDatepicker #d=\"ngbDatepicker\" (click)=\"d.toggle(); e.close();\" type=\"text\" (dateSelect)=\"onDateStartSelect($event)\" [minDate]=\"minDate \"\r\n                [maxDate]=\"maxDate\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n                <div class='input-daterange'style=\"width: 54%;\"> \r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-from\" placeholder=\"End date\" name=\"dq\"[(ngModel)]=\"enddate_model\" ngbDatepicker #e=\"ngbDatepicker\" (click)=\"d.close(); e.toggle()\" type=\"text\" (dateSelect)=\"onDateEndSelect($event);\" [minDate]=\"minDate\"\r\n                [maxDate]=\"maxDate\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n              </div>\r\n            </div>\r\n              <div class=\"page-upto\"><span *ngIf=\"!noTransactionMesage\" > {{pagination.pageno}} of {{totalItems}}</span> <span *ngIf=\"noTransactionMesage\" >0</span><a (click)=\"nextItems('PREVIOUS')\"><img src=\"../src/images/leftarrow.png\" alt=\"\" class=\"pull-right\" /></a> <a (click)=\"nextItems('NEXT')\"><img src=\"../src/images/rightarrow.png\" alt=\"\" class=\"pull-right\" /> </a></div>\r\n            </div>\r\n        </div>\r\n     </div>"

/***/ }),

/***/ "./src/app/dashboard/settlement/recharge-settlement/recharge-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RechargeSettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RechargeSettlementComponent = (function () {
    function RechargeSettlementComponent(commonserve, activateroute, route, base64) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.route = route;
        this.base64 = base64;
        this.tollInfo = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].tollInfo;
        this.companyDetails = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].companyInfo;
        this.operatorinfo = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].operatorDetails;
        this.transaction_logs = {
            activities: []
        };
        this.recharge_logs = {
            activities: []
        };
        this.pagination = {
            "pageno": 1,
            "pagesize": 10
        };
        this.flag = 0;
        this.lastLogs = 0;
        this.getMaxMinDate();
        this.getCurrentDate();
        this.getTodayRechargeLogs();
    }
    RechargeSettlementComponent.prototype.onDateStartSelect = function (startchange) {
        console.log("startchange chnge......jjjj", this.startchange);
        console.log("start date", startchange);
        if (JSON.stringify(this.startchange) == JSON.stringify(startchange)) {
        }
        else {
            this.pagination.pageno = 1;
        }
        this.startchange = {
            "year": String(startchange.year).padStart(2, '0'),
            "month": String(startchange.month).padStart(2, '0'),
            "day": String(startchange.day).padStart(2, '0')
        };
    };
    RechargeSettlementComponent.prototype.getMaxMinDate = function () {
        var date = new Date();
        this.maxDate = {
            "year": date.getFullYear(),
            "month": date.getMonth() + 1,
            "day": date.getDate()
        };
        var priorDateInMilisecond = date.setMonth(date.getMonth() - 5);
        var priorDate = new Date(priorDateInMilisecond);
        this.minDate = {
            "year": priorDate.getFullYear(),
            "month": priorDate.getMonth(),
            "day": priorDate.getDate()
        };
    };
    RechargeSettlementComponent.prototype.onDateEndSelect = function (endchange) {
        var _this = this;
        if (!this.startchange) {
            alert("Please enter Start Date");
        }
        else {
            this.lastSelectDate = endchange;
            this.lastLogs = 1;
            var today = new Date();
            var ISOString = today.toISOString();
            var todayDate = this.getDate(ISOString);
            var endchangeTemp = String(endchange.year).padStart(2, '0') + "-" + String(endchange.month).padStart(2, '0') + "-" + String(endchange.day).padStart(2, '0');
            if (endchangeTemp == todayDate) {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day + 1).padStart(2, '0')
                };
            }
            else {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day).padStart(2, '0')
                };
            }
            var tempEndChange = this.endchange;
            tempEndChange = {
                "year": String(endchange.year).padStart(2, '0'),
                "month": String(endchange.month).padStart(2, '0'),
                "day": String(endchange.day + 1).padStart(2, '0')
            };
            console.log("this.endchange", tempEndChange);
            if (JSON.stringify(this.endchange) !== JSON.stringify(tempEndChange) && this.flag == 1) {
                this.pagination.pageno = 1;
            }
            var queryparam = "?time_slice=lastthreemonths&start_time=" + this.startchange.year + "-" + this.startchange.month + "-" + this.startchange.day + "&end_time=" + this.endchange.year + "-" + this.endchange.month + "-" + this.endchange.day + "&page_size=10&action=CARD_TOP_UP" + "&page=" + this.pagination.pageno;
            this.commonserve.transactionLogsApi(queryparam)
                .subscribe(function (response) {
                console.log("recharge log working with date chnge......", response);
                if (response['totalItems'] == 0) {
                    _this.noTransactionMesage = "No Transaction";
                }
                else {
                    _this.noTransactionMesage = "";
                    _this.recharge_logs = response;
                    console.log("recharge log working......", _this.recharge_logs.activities);
                    _this.totalItems = _this.recharge_logs['totalPages'];
                }
            });
        }
    };
    //convert date of Tz format and show in UI
    RechargeSettlementComponent.prototype.getDate = function (item) {
        var temp = String(item).split('T');
        var date = temp[0];
        return date;
    };
    //ends
    //restrict customer to select date with 6 months
    RechargeSettlementComponent.prototype.getCurrentDate = function () {
        var today = new Date();
        this.toISOString = today.toISOString();
        var dd = String(today.getDate()).padStart(2, '0');
        var nextDay = String(today.getDate() - 1).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        this.currentDate = {
            "dd": dd,
            "nextDay": nextDay,
            "mm": mm,
            "yyyy": yyyy
        };
        today.setMonth(today.getMonth() - 3);
        var localdate = today.toLocaleDateString();
        var arrayStartDate = localdate.split('/');
        var startday = arrayStartDate[0].padStart(2, '0');
        var startmonth = arrayStartDate[1].padStart(2, '0');
        var startyear = arrayStartDate[2];
        this.threeMonthPrior = {
            "mm": startday,
            "dd": startmonth,
            "yyyy": startyear
        };
        console.log("three month before", this.threeMonthPrior);
    };
    //get recharge transaction logs
    RechargeSettlementComponent.prototype.getTodayRechargeLogs = function () {
        var _this = this;
        var queryparam = "?time_slice=lastthreemonths&start_time=" + this.currentDate.yyyy + "-" + this.currentDate.mm + "-" + this.currentDate.dd + "&end_time=" + this.toISOString + "&page_size=" + this.pagination.pagesize + "&action=CARD_TOP_UP" + "&page=" + this.pagination.pageno;
        this.commonserve.transactionLogsApi(queryparam)
            .subscribe(function (response) {
            if (response['totalPages'] == 0) {
                _this.noTransactionMesage = "No Transaction";
            }
            else {
                _this.noTransactionMesage = "";
                _this.recharge_logs = response;
                console.log("recharge log working......", _this.recharge_logs.activities);
                _this.totalItems = _this.recharge_logs['totalPages'];
            }
        });
    };
    //ends
    RechargeSettlementComponent.prototype.nextItems = function (type) {
        if (type == 'NEXT') {
            if (this.pagination.pageno < this.totalItems) {
                this.pagination.pageno = this.pagination.pageno + 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayRechargeLogs();
            }
        }
        else {
            if (this.pagination.pageno > 1) {
                this.pagination.pageno = this.pagination.pageno - 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayRechargeLogs();
            }
        }
    };
    RechargeSettlementComponent.prototype.showUnSuccessfulRecharge = function () {
        this.recharge_logs = {
            activities: []
        };
        this.noTransactionMesage = "No Transaction";
    };
    RechargeSettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/recharge-settlement/recharge-settlement.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3__services_base64_service__["a" /* Base64Service */]])
    ], RechargeSettlementComponent);
    return RechargeSettlementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/settlement.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettlementModule", function() { return SettlementModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__card_settlement_card_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/card-settlement/card-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__ = __webpack_require__("./src/app/dashboard/settlement/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rate_card_settlement_rate_card_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/rate-card-settlement/rate-card-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__transaction_settlement_transaction_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/transaction-settlement/transaction-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pass_settlement_pass_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/pass-settlement/pass-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__recharge_settlement_recharge_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/recharge-settlement/recharge-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__summary_settlement_summary_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/summary-settlement/summary-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__settlement_routing_module__ = __webpack_require__("./src/app/dashboard/settlement/settlement.routing-module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var SettlementModule = (function () {
    function SettlementModule() {
    }
    SettlementModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_10__settlement_routing_module__["a" /* SettlementRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_11__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__card_settlement_card_settlement_component__["a" /* CardSettlementComponent */],
                __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_5__rate_card_settlement_rate_card_settlement_component__["a" /* RateCardSettlementComponent */],
                __WEBPACK_IMPORTED_MODULE_6__transaction_settlement_transaction_settlement_component__["a" /* TransactionSettlementComponent */],
                __WEBPACK_IMPORTED_MODULE_7__pass_settlement_pass_settlement_component__["a" /* PassSettlementComponent */],
                __WEBPACK_IMPORTED_MODULE_8__recharge_settlement_recharge_settlement_component__["a" /* RechargeSettlementComponent */],
                __WEBPACK_IMPORTED_MODULE_9__summary_settlement_summary_settlement_component__["a" /* SummarySettlementComponent */]
            ],
            exports: []
        })
    ], SettlementModule);
    return SettlementModule;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/settlement.routing-module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettlementRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_settlement_card_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/card-settlement/card-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile_component__ = __webpack_require__("./src/app/dashboard/settlement/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__rate_card_settlement_rate_card_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/rate-card-settlement/rate-card-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__transaction_settlement_transaction_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/transaction-settlement/transaction-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pass_settlement_pass_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/pass-settlement/pass-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__recharge_settlement_recharge_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/recharge-settlement/recharge-settlement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__summary_settlement_summary_settlement_component__ = __webpack_require__("./src/app/dashboard/settlement/summary-settlement/summary-settlement.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__card_settlement_card_settlement_component__["a" /* CardSettlementComponent */] },
    { path: 'cardsettlement', component: __WEBPACK_IMPORTED_MODULE_2__card_settlement_card_settlement_component__["a" /* CardSettlementComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_3__profile_profile_component__["a" /* ProfileComponent */] },
    { path: 'rate-card', component: __WEBPACK_IMPORTED_MODULE_4__rate_card_settlement_rate_card_settlement_component__["a" /* RateCardSettlementComponent */] },
    { path: 'transaction', component: __WEBPACK_IMPORTED_MODULE_5__transaction_settlement_transaction_settlement_component__["a" /* TransactionSettlementComponent */] },
    { path: 'pass', component: __WEBPACK_IMPORTED_MODULE_6__pass_settlement_pass_settlement_component__["a" /* PassSettlementComponent */] },
    { path: 'recharge', component: __WEBPACK_IMPORTED_MODULE_7__recharge_settlement_recharge_settlement_component__["a" /* RechargeSettlementComponent */] },
    { path: 'summary', component: __WEBPACK_IMPORTED_MODULE_8__summary_settlement_summary_settlement_component__["a" /* SummarySettlementComponent */] },
];
var SettlementRoutingModule = (function () {
    function SettlementRoutingModule() {
    }
    SettlementRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], SettlementRoutingModule);
    return SettlementRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/summary-settlement/summary-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n    <div class=\"button-style\">\r\n      <a [routerLink]=\"['../rate-card']\">\r\n        <button type=\"button\">Rate Card</button>\r\n      </a>\r\n      <a [routerLink]=\"['../transaction']\">\r\n        <button type=\"button\" class=\"\">Transaction</button>\r\n      </a>\r\n      <a [routerLink]=\"['../pass']\">\r\n        <button type=\"button\" class=\"\">Pass</button>\r\n      </a>\r\n      <a [routerLink]=\"['../recharge']\">\r\n        <button type=\"button\" class=\"\">Recharge</button>\r\n      </a>\r\n      <a [routerLink]=\"['../cardsettlement']\">\r\n        <button type=\"button\" name=\"\">Doe Card</button>\r\n      </a>\r\n      <a>\r\n        <button type=\"button\" class=\"active\" name=\"\">Summary</button>\r\n      </a>\r\n      <a [routerLink]=\"['../profile']\">\r\n        <button type=\"button\" name=\"\">Profile</button>\r\n      </a>\r\n    </div>\r\n        <div class=\"content-page\">\r\n            <div class=\"scrollable background-summary-table\">\r\n              <!-- first table for all transaction -->\r\n              <span class=\"summary-sub-headers\">ALL TRANSACTION</span>\r\n                <div class=\"fourth-scroll scroll-style \">\r\n                    <table class=\"recharge-transaction-table left-adjust\">\r\n                        <tr>\r\n                            <th></th>\r\n                            <th>COUNT</th>\r\n                            <th>TOTAL AMOUNT</th>\r\n                            <th>INSENTIVE</th>\r\n                            <th>GST</th>\r\n                            <th>TDS</th>\r\n                            <th>NET AMOUNT</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Sale</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Passes</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Recharge</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Doe cards</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                    </table>\r\n                </div>\r\n              <!-- first table for all transaction ends-->\r\n              <!-- second table for all transaction -->\r\n              <span class=\"summary-sub-headers\">CREDIT AND DEBIT DETAILS</span>\r\n              <div class=\"fourth-scroll scroll-style \">\r\n                    <table class=\"recharge-transaction-table left-adjust\">\r\n                        <tr>\r\n                            <th></th>\r\n                            <th>COUNT</th>\r\n                            <th>TOTAL AMOUNT</th>\r\n                            <th>INSENTIVE</th>\r\n                            <th>GST</th>\r\n                            <th>TDS</th>\r\n                            <th>NET AMOUNT</th>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Sale</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Passes</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Recharge</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Doe cards</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                            <td>100</td>\r\n                        </tr>\r\n                    </table>\r\n                </div>\r\n               <!-- second table for all transaction ends--> \r\n               <!-- third table section ends -->\r\n               <span class=\"summary-sub-headers\">TOTAL AMOUNT TRANSFER DETAILS</span>\r\n              <div class=\"total-amount-transafer-section \">\r\n                 <div class=\"escrow-merchant-balance\">\r\n                   <span>Amount deposited to escrow account</span>\r\n                   <p>5555</p>\r\n                 </div>\r\n                 <div class=\"escrow-merchant-balance\">\r\n                   <span>Balance goes to merchant account</span>\r\n                   <p>5555</p>\r\n                 </div>\r\n              </div>\r\n                <!-- third table section ends -->\r\n            </div>\r\n            <!--footer-->\r\n            <div class=\"settlement-buttons\">\r\n              <button type=\"button\" [routerLink]=\"['../../../dashboard']\" name=\"button\" class=\"dashboard-meter pull-left\">\r\n                <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n                <p>Dashboard</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\" (click)=\"showtime()\">\r\n                <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n                <p>Settlements</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"send-report pull-left\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n                <p>Send Report</p>\r\n              </button>\r\n              <div class=\"profile-calender\" style=\"height: 100%; position: relative; width: 23%;\" >\r\n              <div style=\"display:flex; flex-direction: row; justify-content:space-between; height: 100%;width: 100%;\">\r\n                <div class='input-daterange ' style=\"width: 54%;\">\r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-to\" placeholder=\"Start date\" name=\"dp\"[(ngModel)]=\"startdate_model\" ngbDatepicker #d=\"ngbDatepicker\" (click)=\"d.toggle(); e.close();\" type=\"text\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n                <div class='input-daterange'style=\"width: 54%;\"> \r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-from\" placeholder=\"End date\" name=\"dq\"[(ngModel)]=\"enddate_model\" ngbDatepicker #e=\"ngbDatepicker\" (click)=\"d.close(); e.toggle();\" type=\"text\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n              </div>\r\n            </div>\r\n              <div class=\"page-upto\"><span> 1-19 of 100</span><img src=\"../src/images/leftarrow.png\" alt=\"\" class=\"pull-right\" /> <img src=\"../src/images/rightarrow.png\" alt=\"\" class=\"pull-right\" /> </div>\r\n            </div>\r\n        </div>\r\n     </div>"

/***/ }),

/***/ "./src/app/dashboard/settlement/summary-settlement/summary-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SummarySettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SummarySettlementComponent = (function () {
    function SummarySettlementComponent() {
    }
    SummarySettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/summary-settlement/summary-settlement.component.html"),
        })
    ], SummarySettlementComponent);
    return SummarySettlementComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/settlement/transaction-settlement/transaction-settlement.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-class\">\r\n        <div class=\"button-style\">\r\n          <a [routerLink]=\"['../rate-card']\">\r\n            <button type=\"button\">Rate Card</button>\r\n          </a>\r\n          <a>\r\n            <button type=\"button\" class=\"active\">Transaction</button>\r\n          </a>\r\n          <a [routerLink]=\"['../pass']\">\r\n            <button type=\"button\" class=\"\">Pass</button>\r\n          </a>\r\n          <a [routerLink]=\"['../recharge']\">\r\n            <button type=\"button\" class=\"\">Recharge</button>\r\n          </a>\r\n          <a [routerLink]=\"['../cardsettlement']\">\r\n            <button type=\"button\" name=\"\">Doe Card</button>\r\n          </a>\r\n          <a [routerLink]=\"['../summary']\">\r\n            <button type=\"button\" name=\"\">Summary</button>\r\n          </a>\r\n          <a [routerLink]=\"['../profile']\">\r\n            <button type=\"button\" name=\"\">Profile</button>\r\n          </a>\r\n        </div>\r\n        <div class=\"content-page\">\r\n            <div *ngIf=\"noTransactionMesage\" class=\"amounts-div-header no-margin-to\">\r\n                <span>{{noTransactionMesage}} </span>\r\n            </div>\r\n            <div class=\"scrollable\" *ngIf=\"!noTransactionMesage\">\r\n                <table class=\"first-transaction-table\">\r\n                    <tr>\r\n                        <th>MERCHANT NAME</th>\r\n                        <th>TOLL GATE DETAILS</th>\r\n                        <th>OPERATOR DETAILS</th>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>\r\n                            <p>Name of Concessionaire / OMT Contractor : {{companyDetails.name}}.<br> Name / Contact Details of Incharge : {{companyDetails.contact.details.landlineNumber}}</p>\r\n                        </td>\r\n                        <td>\r\n                            <p>{{tollInfo.items[0].name}}<br>{{tollInfo.items[0].location.area}}<br>Km {{tollInfo.items[0].location.kiloMeter}}.{{tollInfo.items[0].location.highwayType}} IN {{tollInfo.items[0].location.state}}<br>Stretch : {{tollInfo.items[0].location.stretch}}<br>Tollable Length : {{tollInfo.items[0].location.tollableLength}}\r\n                            </p>\r\n                        </td>\r\n                        <td>Id : {{operatorinfo.loginIdentifiers.username.identifier}}<br> Operator Name : {{operatorinfo.name}}</td>\r\n                </table>\r\n                <div *ngIf=\"noTransactionMesage\" class=\"noTransactionMessage\">\r\n                    <span>{{noTransactionMesage}} </span>\r\n                </div>\r\n                <div class=\"second-scroll scroll-style\" *ngIf=\"!noTransactionMesage\">\r\n                    <table class=\"second-transaction-table left-adjust\">\r\n                        <tr>\r\n                            <th>S NO.</th>\r\n                            <th>CARD NUMBER</th>\r\n                            <th>DATE</th>\r\n                            <th>VEHICLE NUMBER</th>\r\n                            <th>VEHICLE TYPE</th>\r\n                            <th>ENTRY TIME</th>\r\n                            <th>EXIT TIME</th>\r\n                            <th>JOURNEY TYPE</th>\r\n                            <th>AMOUNT</th>\r\n                        </tr>\r\n                        <tr *ngFor=\"let item of transaction_logs.activities; let i = index\">\r\n                            <td *ngIf=\"!item.id\">NA </td> <td *ngIf=\"item.id\">{{i+1}}</td>\r\n                            <td *ngIf=\"!item.cardNumber\">NA</td> <td *ngIf=\"item.cardNumber\">{{item.cardNumber}}</td>\r\n                            <td *ngIf=\"!item.activityTime\">NA</td> <td *ngIf=\"item.activityTime\">{{getDate(item.activityTime)}}</td>\r\n                            <td *ngIf=\"!item.vehicleNo\">NA</td> <td *ngIf=\"item.vehicleNo\">{{item.vehicleNo}}</td>\r\n                            <td *ngIf=\"!item.vehicletype\">NA</td> <td *ngIf=\"item.vehicletype\">{{item.vehicletype}}</td>\r\n                            <td *ngIf=\"!item.activityTime\">NA</td> <td *ngIf=\"item.activityTime\">{{formatAMPM(item.activityTime)}}</td>\r\n                            <td *ngIf=\"!item.activityTime\">NA</td> <td *ngIf=\"item.activityTime\">{{formatAMPM(item.activityTime)}}</td>\r\n                            <td *ngIf=\"!item.action\">NA</td> <td *ngIf=\"item.action\">{{item.action}}</td>\r\n                            <td *ngIf=\"!item.amount.value\">&#8377; NA</td> <td *ngIf=\"item.amount.value\">&#8377; {{item.amount.value}}</td>\r\n                        </tr>\r\n                    </table>\r\n                </div>\r\n                <div class=\"transaction-amount-div total-amount total-amount-passactivated\" *ngIf=\"!noTransactionMesage\">\r\n                    <div class=\"text-total\"><span class=\"total-amount-text\">Total </span> </div>\r\n                    <div class=\"total-card-sold\"><span class=\"total-card\"></span>--</div>\r\n                    <div style= \"width:66%;\"></div>\r\n                    <div class=\"total-amount-collected\"><span class=\" total-settlement-amount\">&#8377; --</span></div>\r\n                </div>\r\n                <div class=\"total-recharge-div background-orange\" *ngIf=\"!noTransactionMesage\">\r\n                    <div class=\"total-text\"><span class=\"total-amount-text\">Total </span> </div>\r\n                    <div class=\"total-card-recharge\"><span class=\"total-card\">--</span> </div>\r\n                    <div class=\"total-recharge-sum\"><span class=\" recharge-total-amount\">&#8377; -- </span></div>\r\n                </div>\r\n                <!--amount division-->\r\n                <h3 class=\"amounts-div-header width-style\"> \r\n                    AMOUNTS\r\n                </h3>\r\n                <table class=\"amounts-table last-width \">\r\n                    <tr>\r\n                        <td class=\"td-left\">TOTAL TRANSACTION AMOUNT</td>\r\n                        <td>&#8377; --</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"td-left\">TOTAL RECHARGE AMOUNT</td>\r\n                        <td>&#8377; --</td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"amount-style-bottom width-style total-amount-footer\">\r\n                    <div class=\"total-collection-text\"><span class=\"total-amounts-amount\">Total </span></div> \r\n                    <div class=\"total-collection-text\"><span class=\" amounts-amount\">&#8377; --</span></div>\r\n                </div>\r\n                <table class=\"amounts-table last-width \">\r\n                    <tr>\r\n                        <td class=\"td-left\">AMOUNT DEPOSITED TO ESCROW ACCOUNT</td>\r\n                        <td>&#8377; --</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"td-left\">BALANCE GOES TO MERCHANT ACCOUNT</td>\r\n                        <td>&#8377; --</td>\r\n                    </tr>\r\n                </table>\r\n                <!--transaction full details-->\r\n                <table class=\"transaction-full-detail last-width\" *ngIf=\"companyInfo\">\r\n                    <tr>\r\n                        <td class=\"details-left\">Company Name</td>\r\n                        <td class=\"details-right\">{{companyInfo.name}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">Name of Account</td>\r\n                        <td class=\"details-right\">{{companyInfo.registration.details.registeredName}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">Phone Number</td>\r\n                        <td class=\"details-right\">{{companyInfo.contact.details.landlineNumber}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">BANK Name</td>\r\n                        <td class=\"details-right\">{{companyInfo.settlement.details.bankAccount.bankName}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">Account Number</td>\r\n                        <td class=\"details-right\">{{companyInfo.settlement.details.bankAccount.accountNumber}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">Name of Account</td>\r\n                        <td class=\"details-right\">{{companyInfo.settlement.details.bankAccount.type}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"details-left\">IFSC Code</td>\r\n                        <td class=\"details-right\">{{companyInfo.settlement.details.bankAccount.IFSC}}</td>\r\n                    </tr>\r\n                </table>\r\n                <div class=\"details-details-total width-style total-amount-footer\">\r\n                    <div><span class=\"details-amount-text\">Total </span> <span class=\"details-amount-total\">--</span> </div>\r\n                </div>\r\n                <div class=\"report-generated-div last-width\">\r\n                    <div class=\"start-date\"><span class=\"report-generation-from\">Reports Generation From </span> <span class=\"date-from\">MAY-12-2016 12.00</span> <span class=\"to-text\">TO</span><span class=\"date-to\">MAY-12-2016 12.00</span></div>\r\n                </div>\r\n                <div class=\"report-generated-div last-width\">\r\n                    <div class=\"start-date\"><span class=\"report-generation-from\">Automatic Settlement From </span> <span class=\"date-from\">MAY-12-2016 12.00</span> <span class=\"to-text\">TO</span><span class=\"date-to\">MAY-12-2016 12.00</span></div>\r\n                </div>\r\n            </div>\r\n            <!--footer-->\r\n            <div class=\"settlement-buttons\">\r\n              <button type=\"button\" [routerLink]=\"['../../../dashboard']\" name=\"button\" class=\"dashboard-meter pull-left\">\r\n                <img src=\"../src/images/black-dashboard-icon.png\" alt=\"\" />\r\n                <p>Dashboard</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"settlement-meter pull-left\" (click)=\"showtime()\">\r\n                <img src=\"../src/images/white-settlements-icon.png\" alt=\"\" />\r\n                <p>Settlements</p>\r\n              </button>\r\n              <button type=\"button\" name=\"button\" class=\"send-report pull-left\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModal\">\r\n                <p>Send Report</p>\r\n              </button>\r\n              <div class=\"profile-calender\" style=\"height: 100%; position: relative; width: 23%;\" >\r\n              <div style=\"display:flex; flex-direction: row; justify-content:space-between; height: 100%;width: 100%;\">\r\n                <div class='input-daterange ' style=\"width: 54%;\">\r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-to\" placeholder=\"Start date\" name=\"dp\"[(ngModel)]=\"startdate_model\" ngbDatepicker #d=\"ngbDatepicker\" (click)=\"d.toggle(); e.close();\" type=\"text\" (dateSelect)=\"onDateStartSelect($event)\" [minDate]=\"minDate \"\r\n                [maxDate]=\"maxDate\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n                <div class='input-daterange'style=\"width: 54%;\"> \r\n                  <!-- ngb date -->\r\n                  <input class=\"calender-from\" placeholder=\"End date\" name=\"dq\"[(ngModel)]=\"enddate_model\" ngbDatepicker #e=\"ngbDatepicker\" (click)=\"d.close(); e.toggle()\" type=\"text\" (dateSelect)=\"onDateEndSelect($event)\" [minDate]=\"minDate\"\r\n                [maxDate]=\"maxDate\" readonly>\r\n                  <!-- ngb date -->\r\n                </div>\r\n              </div>\r\n            </div>\r\n              <div class=\"page-upto\"><span *ngIf=\"!noTransactionMesage\" > {{pagination.pageno}} of {{totalItems}}</span> <span *ngIf=\"noTransactionMesage\" >0</span><a (click)=\"nextItems('PREVIOUS')\"><img src=\"../src/images/leftarrow.png\" alt=\"\" class=\"pull-right\" /></a> <a (click)=\"nextItems('NEXT')\"><img src=\"../src/images/rightarrow.png\" alt=\"\" class=\"pull-right\" /> </a></div>\r\n        </div>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/settlement/transaction-settlement/transaction-settlement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionSettlementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_base64_service__ = __webpack_require__("./src/app/services/base64.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TransactionSettlementComponent = (function () {
    function TransactionSettlementComponent(commonserve, activateroute, route, base64) {
        this.commonserve = commonserve;
        this.activateroute = activateroute;
        this.route = route;
        this.base64 = base64;
        this.tollInfo = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].tollInfo;
        this.companyDetails = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].companyInfo;
        this.operatorinfo = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].operatorDetails;
        this.transaction_logs = {
            activities: []
        };
        this.recharge_logs = {
            activities: []
        };
        this.pagination = {
            "pageno": 1,
            "pagesize": 10
        };
        this.flag = 0;
        this.lastLogs = 0;
        this.lastLogs = 0;
        this.getCurrentDate();
        this.getMaxMinDate();
        this.getTodayTransactionLogs();
    }
    //restrict customer to select date with 6 months
    TransactionSettlementComponent.prototype.getMaxMinDate = function () {
        var date = new Date();
        this.maxDate = {
            "year": date.getFullYear(),
            "month": date.getMonth() + 1,
            "day": date.getDate()
        };
        var priorDateInMilisecond = date.setMonth(date.getMonth() - 5);
        var priorDate = new Date(priorDateInMilisecond);
        this.minDate = {
            "year": priorDate.getFullYear(),
            "month": priorDate.getMonth(),
            "day": priorDate.getDate()
        };
    };
    //ends
    //convert date of Tz format and show in UI
    TransactionSettlementComponent.prototype.getDate = function (item) {
        var temp = String(item).split('T');
        var date = temp[0];
        return date;
    };
    //ends
    TransactionSettlementComponent.prototype.formatAMPM = function (date) {
        var d = new Date(date);
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    TransactionSettlementComponent.prototype.getCurrentDate = function () {
        var today = new Date();
        this.toISOString = today.toISOString();
        var dd = String(today.getDate()).padStart(2, '0');
        var nextDay = String(today.getDate() - 1).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        this.currentDate = {
            "dd": dd,
            "nextDay": nextDay,
            "mm": mm,
            "yyyy": yyyy
        };
        today.setMonth(today.getMonth() - 3);
        var localdate = today.toLocaleDateString();
        var arrayStartDate = localdate.split('/');
        var startday = arrayStartDate[0].padStart(2, '0');
        var startmonth = arrayStartDate[1].padStart(2, '0');
        var startyear = arrayStartDate[2];
        this.threeMonthPrior = {
            "mm": startday,
            "dd": startmonth,
            "yyyy": startyear
        };
        console.log("three month before", this.threeMonthPrior);
    };
    TransactionSettlementComponent.prototype.nextItems = function (type) {
        if (type == 'NEXT') {
            if (this.pagination.pageno < this.totalItems) {
                this.pagination.pageno = this.pagination.pageno + 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayTransactionLogs();
            }
        }
        else {
            if (this.pagination.pageno > 1) {
                this.pagination.pageno = this.pagination.pageno - 1;
                if (this.lastLogs == 1)
                    this.onDateEndSelect(this.lastSelectDate);
                else
                    this.getTodayTransactionLogs();
            }
        }
    };
    //ends
    //get logs for selected dates
    TransactionSettlementComponent.prototype.onDateEndSelect = function (endchange) {
        var _this = this;
        if (!this.startchange) {
            alert("Please enter Start Date");
        }
        else {
            this.lastSelectDate = endchange;
            this.lastLogs = 1;
            console.log("end date", endchange);
            var today = new Date();
            var ISOString = today.toISOString();
            var todayDate = this.getDate(ISOString);
            var endchangeTemp = String(endchange.year).padStart(2, '0') + "-" + String(endchange.month).padStart(2, '0') + "-" + String(endchange.day).padStart(2, '0');
            if (endchangeTemp == todayDate) {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day + 1).padStart(2, '0')
                };
            }
            else {
                this.endchange = {
                    "year": String(endchange.year).padStart(2, '0'),
                    "month": String(endchange.month).padStart(2, '0'),
                    "day": String(endchange.day).padStart(2, '0')
                };
            }
            var tempEndChange = this.endchange;
            tempEndChange = {
                "year": String(endchange.year).padStart(2, '0'),
                "month": String(endchange.month).padStart(2, '0'),
                "day": String(endchange.day + 1).padStart(2, '0')
            };
            console.log("this.endchange", tempEndChange);
            if (JSON.stringify(this.endchange) !== JSON.stringify(tempEndChange) && this.flag == 1) {
                this.pagination.pageno = 1;
            }
            var queryparam = "?time_slice=lastthreemonths&start_time=" + this.startchange.year + "-" + this.startchange.month + "-" + this.startchange.day + "&end_time=" + this.endchange.year + "-" + this.endchange.month + "-" + this.endchange.day + "&page_size=10&action=TOLL_ENTRY" + "&page=" + this.pagination.pageno;
            this.commonserve.transactionLogsApi(queryparam)
                .subscribe(function (response) {
                if (response['totalPages'] == 0) {
                    _this.noTransactionMesage = "No Transaction";
                }
                else {
                    _this.noTransactionMesage = "";
                    _this.transaction_logs = response;
                    console.log("recharge log working......", _this.recharge_logs.activities);
                    _this.totalItems = _this.transaction_logs['totalPages'];
                }
            });
        }
    };
    //to get start date from ngbdatepicker
    TransactionSettlementComponent.prototype.onDateStartSelect = function (startchange) {
        console.log("start date", startchange);
        //this.startchange = startchange;
        if (JSON.stringify(this.startchange) == JSON.stringify(startchange)) {
        }
        else {
            this.pagination.pageno = 1;
        }
        this.startchange = {
            "year": String(startchange.year).padStart(2, '0'),
            "month": String(startchange.month).padStart(2, '0'),
            "day": String(startchange.day).padStart(2, '0')
        };
    };
    //ends
    //get today transaction logs
    TransactionSettlementComponent.prototype.getTodayTransactionLogs = function () {
        var _this = this;
        var queryparam = "?time_slice=lastthreemonths&start_time=" + this.currentDate.yyyy + "-" + this.currentDate.mm + "-" + this.currentDate.dd + "&end_time=" + this.toISOString + "&page_size=" + this.pagination.pagesize + "&action=TOLL_ENTRY" + "&page=" + this.pagination.pageno;
        this.commonserve.transactionLogsApi(queryparam)
            .subscribe(function (response) {
            if (response['totalPages'] == 0) {
                _this.noTransactionMesage = "No Transaction";
            }
            else {
                _this.noTransactionMesage = "";
                _this.transaction_logs = response;
                console.log("recharge log working......", _this.recharge_logs.activities);
                _this.totalItems = _this.transaction_logs['totalPages'];
            }
        });
    };
    TransactionSettlementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/dashboard/settlement/transaction-settlement/transaction-settlement.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonBaseService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4__services_base64_service__["a" /* Base64Service */]])
    ], TransactionSettlementComponent);
    return TransactionSettlementComponent;
}());



/***/ })

});
//# sourceMappingURL=settlement.module.chunk.js.map